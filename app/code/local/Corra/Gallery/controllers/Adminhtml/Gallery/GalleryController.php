<?php

/**
 * Gallery admin controller
 *
 * @category    Corra
 * @package     Corra_Gallery
 */
class Corra_Gallery_Adminhtml_Gallery_GalleryController extends Corra_Gallery_Controller_Adminhtml_Gallery
{

    /**
     * init the gallery
     *
     * @access protected
     * @return Corra_Gallery_Model_Gallery
     */
    protected function _initGallery()
    {
        $galleryId = (int) $this->getRequest()->getParam('id');
        $gallery = Mage::getModel('corra_gallery/gallery');
        if ($galleryId) {
            $gallery->load($galleryId);
        }
        Mage::register('current_gallery', $gallery);
        return $gallery;
    }

    /**
     * default action
     *
     * @access public
     * @return void
     */
    public function indexAction()
    {
        $this->loadLayout();
        $this->_title(Mage::helper('corra_gallery')->__('Gallery Images'))
            ->_title(Mage::helper('corra_gallery')->__('Galler Images'));
        $this->renderLayout();
    }

    /**
     * grid action
     *
     * @access public
     * @return void
     */
    public function gridAction()
    {
        $this->loadLayout()->renderLayout();
    }

    /**
     * edit gallery - action
     *
     * @access public
     * @return void
     */
    public function editAction()
    {
        $galleryId = $this->getRequest()->getParam('id');
        $gallery = $this->_initGallery();
        if ($galleryId && !$gallery->getId()) {
            $this->_getSession()->addError(
                Mage::helper('corra_gallery')->__('This gallery no longer exists.')
            );
            $this->_redirect('*/*/');
            return;
        }
        $data = Mage::getSingleton('adminhtml/session')->getGalleryData(true);
        if (!empty($data)) {
            $gallery->setData($data);
        }
        Mage::register('gallery_data', $gallery);
        $this->loadLayout();
        $this->_title(Mage::helper('corra_gallery')->__('Gallery Images'))
            ->_title(Mage::helper('corra_gallery')->__('Gallery Images'));
        if ($gallery->getId()) {
            $this->_title($gallery->getTitle());
        } else {
            $this->_title(Mage::helper('corra_gallery')->__('Add Gallery Images'));
        }
        if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
            $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
        }
        $this->renderLayout();
    }

    /**
     * new gallery action
     *
     * @access public
     * @return void
     */
    public function newAction()
    {
        $this->_forward('edit');
    }

    /**
     * save gallery - action
     *
     * @access public
     * @return void
     */
    public function saveAction()
    {
        if ($data = $this->getRequest()->getPost('gallery')) {
            try {
                $gallery = $this->_initGallery();
                $gallery->addData($data);
                $desktopImageName = $this->_uploadAndGetName(
                    'desktop_image', Mage::helper('corra_gallery/gallery_image')->getImageBaseDir(), $data
                );
                $gallery->setData('desktop_image', $desktopImageName);
                $tabletImageName = $this->_uploadAndGetName(
                    'tablet_image', Mage::helper('corra_gallery/gallery_image')->getImageBaseDir(), $data
                );
                $gallery->setData('tablet_image', $tabletImageName);
                $mobileImageName = $this->_uploadAndGetName(
                    'mobile_image', Mage::helper('corra_gallery/gallery_image')->getImageBaseDir(), $data
                );
                $gallery->setData('mobile_image', $mobileImageName);
                $gallery->save();
                if(count($data['hotspot'])>0){
                    
                    $hotspot = Mage::getModel('corra_gallery/hotspot');
                    $collection = $hotspot->getCollection()->addFieldToFilter('gallery_id',$gallery->getId());
                    foreach ($collection as $_item) :
                        $hotspot->setId($_item->getId())->delete();
                    endforeach;
                    
                    $hotspotData = $data['hotspot'];                    
                    foreach ($hotspotData as $item){
                        $itemData['xvalue']=$item['xvalue'];
                        $itemData['yvalue']=$item['yvalue'];
                        $itemData['sku']=$item['sku'];
                        $itemData['gallery_id']=$gallery->getId();
                        $hotspot = Mage::getModel('corra_gallery/hotspot');
                        $hotspot->addData($itemData);
                        $hotspot->save();
                        unset($itemData);
                    }                    
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('corra_gallery')->__('Gallery was successfully saved')
                );
                Mage::getSingleton('adminhtml/session')->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $gallery->getId()));
                    return;
                }
                $this->_redirect('*/*/');
                return;
            } catch (Mage_Core_Exception $e) {
                if (isset($data['desktop_image']['value'])) {
                    $data['desktop_image'] = $data['desktop_image']['value'];
                }
                if (isset($data['tablet_image']['value'])) {
                    $data['tablet_image'] = $data['tablet_image']['value'];
                }
                if (isset($data['mobile_image']['value'])) {
                    $data['mobile_image'] = $data['mobile_image']['value'];
                }
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setGalleryData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            } catch (Exception $e) {
                Mage::logException($e);
                if (isset($data['desktop_image']['value'])) {
                    $data['desktop_image'] = $data['desktop_image']['value'];
                }
                if (isset($data['tablet_image']['value'])) {
                    $data['tablet_image'] = $data['tablet_image']['value'];
                }
                if (isset($data['mobile_image']['value'])) {
                    $data['mobile_image'] = $data['mobile_image']['value'];
                }
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('corra_gallery')->__('There was a problem saving the gallery.')
                );
                Mage::getSingleton('adminhtml/session')->setGalleryData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(
            Mage::helper('corra_gallery')->__('Unable to find gallery to save.')
        );
        $this->_redirect('*/*/');
    }

    /**
     * delete gallery - action
     *
     * @access public
     * @return void
     */
    public function deleteAction()
    {
        if ($this->getRequest()->getParam('id') > 0) {
            try {
                $gallery = Mage::getModel('corra_gallery/gallery');
                $gallery->setId($this->getRequest()->getParam('id'))->delete();
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('corra_gallery')->__('Gallery was successfully deleted.')
                );
                $this->_redirect('*/*/');
                return;
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('corra_gallery')->__('There was an error deleting gallery.')
                );
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                Mage::logException($e);
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(
            Mage::helper('corra_gallery')->__('Could not find gallery to delete.')
        );
        $this->_redirect('*/*/');
    }

    /**
     * mass delete gallery - action
     *
     * @access public
     * @return void
     */
    public function massDeleteAction()
    {
        $galleryIds = $this->getRequest()->getParam('gallery');
        if (!is_array($galleryIds)) {
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('corra_gallery')->__('Please select galleries to delete.')
            );
        } else {
            try {
                foreach ($galleryIds as $galleryId) {
                    $gallery = Mage::getModel('corra_gallery/gallery');
                    $gallery->setId($galleryId)->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('corra_gallery')->__('Total of %d galleries were successfully deleted.', count($galleryIds))
                );
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('corra_gallery')->__('There was an error deleting galleries.')
                );
                Mage::logException($e);
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * mass status change - action
     *
     * @access public
     * @return void
     */
    public function massStatusAction()
    {
        $galleryIds = $this->getRequest()->getParam('gallery');
        if (!is_array($galleryIds)) {
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('corra_gallery')->__('Please select galleries.')
            );
        } else {
            try {
                foreach ($galleryIds as $galleryId) {
                    $gallery = Mage::getSingleton('corra_gallery/gallery')->load($galleryId)
                        ->setStatus($this->getRequest()->getParam('status'))
                        ->setIsMassupdate(true)
                        ->save();
                }
                $this->_getSession()->addSuccess(
                    $this->__('Total of %d galleries were successfully updated.', count($galleryIds))
                );
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('corra_gallery')->__('There was an error updating galleries.')
                );
                Mage::logException($e);
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * export as csv - action
     *
     * @access public
     * @return void
     */
    public function exportCsvAction()
    {
        $fileName = 'gallery.csv';
        $content = $this->getLayout()->createBlock('corra_gallery/adminhtml_gallery_grid')
            ->getCsv();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * export as MsExcel - action
     *
     * @access public
     * @return void
     */
    public function exportExcelAction()
    {
        $fileName = 'gallery.xls';
        $content = $this->getLayout()->createBlock('corra_gallery/adminhtml_gallery_grid')
            ->getExcelFile();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * export as xml - action
     *
     * @access public
     * @return void
     */
    public function exportXmlAction()
    {
        $fileName = 'gallery.xml';
        $content = $this->getLayout()->createBlock('corra_gallery/adminhtml_gallery_grid')
            ->getXml();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * Check if admin has permissions to visit related pages
     *
     * @access protected
     * @return boolean
     */
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('corra_gallery/gallery');
    }

}
