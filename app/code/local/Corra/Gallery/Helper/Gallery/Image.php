<?php

/**
 * Gallery image helper
 *
 * @category    Corra
 * @package     Corra_Gallery
 */
class Corra_Gallery_Helper_Gallery_Image extends Corra_Gallery_Helper_Image_Abstract
{

    /**
     * image placeholder
     * @var string
     */
    protected $_placeholder = 'images/placeholder/gallery.jpg';

    /**
     * image subdir
     * @var string
     */
    protected $_subdir = 'gallery';

}
