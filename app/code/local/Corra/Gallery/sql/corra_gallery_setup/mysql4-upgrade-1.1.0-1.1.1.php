<?php
$installer = $this;
$installer->startSetup();
$installer->run("
  DROP TABLE IF EXISTS `{$this->getTable('corra_gallery/hotspot')}`;
  CREATE TABLE `{$this->getTable('corra_gallery/hotspot')}` (
  `hotspot_id` smallint(6) NOT NULL AUTO_INCREMENT,
  `xvalue` smallint(5) DEFAULT '0',
  `yvalue` smallint(5) DEFAULT '0',
  `sku` varchar(255) DEFAULT NULL,
  `gallery_id` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`hotspot_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Corra Gallery Hotspot' ;
");
$installer->endSetup();