<?php

/**
 * Gallery module install script
 *
 * @category    Corra
 * @package     Corra_Gallery
 */
$this->startSetup();
$table = $this->getConnection()
    ->newTable($this->getTable('corra_gallery/gallery'))
    ->addColumn(
        'entity_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity' => true,
        'nullable' => false,
        'primary' => true,
        ), 'Gallery ID'
    )
    ->addColumn(
        'title', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
        'nullable' => false,
        ), 'Title'
    )
    ->addColumn(
        'desktop_image', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(), 'Desktop Image'
    )
    ->addColumn(
        'tablet_image', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(), 'Tablet Image'
    )
    ->addColumn(
        'mobile_image', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(), 'Mobile Image'
    )
    ->addColumn(
        'link', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
        'nullable' => false,
        ), 'URL link for image'
    )
    ->addColumn(
        'post_content', Varien_Db_Ddl_Table::TYPE_TEXT, '64k', array(), 'Content'
    )
    ->addColumn(
        'stores', Varien_Db_Ddl_Table::TYPE_TEXT, '64k', array(
        'nullable' => false,
        ), 'Store View'
    )
    ->addColumn(
        'image_position', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'nullable' => false,
        'unsigned' => true,
        ), 'Sort Order'
    )
    ->addColumn(
        'status', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(), 'Enabled'
    )
    ->addColumn(
        'updated_at', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(), 'Gallery Modification Time'
    )
    ->addColumn(
        'created_at', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(), 'Gallery Creation Time'
    )
    ->setComment('Gallery Table');
$this->getConnection()->createTable($table);
$this->endSetup();
