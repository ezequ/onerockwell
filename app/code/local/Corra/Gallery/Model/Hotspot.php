<?php

/**
 * Gallery model
 *
 * @category    Corra
 * @package     Corra_Gallery
 */
class Corra_Gallery_Model_Hotspot extends Mage_Core_Model_Abstract
{

    /**
     * Entity code.
     * Can be used as part of method name for entity processing
     */
    const ENTITY = 'corra_gallery_hotspot';
    const CACHE_TAG = 'corra_gallery_hotspot';

    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_eventPrefix = 'corra_gallery_hotspot';

    /**
     * Parameter name in event
     *
     * @var string
     */
    protected $_eventObject = 'hotspot';

    /**
     * constructor
     *
     * @access public
     * @return void
     */
    public function _construct()
    {
        parent::_construct();
        $this->_init('corra_gallery/hotspot');
    }

    /**
     * before save gallery
     *
     * @access protected
     * @return Corra_Gallery_Model_Gallery
     */
    protected function _beforeSave()
    {
        parent::_beforeSave();        
        return $this;
    }

    

    /**
     * save gallery relation
     *
     * @access public
     * @return Corra_Gallery_Model_Gallery
     */
    protected function _afterSave()
    {
        return parent::_afterSave();
    }

    

}
