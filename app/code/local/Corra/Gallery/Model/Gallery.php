<?php

/**
 * Gallery model
 *
 * @category    Corra
 * @package     Corra_Gallery
 */
class Corra_Gallery_Model_Gallery extends Mage_Core_Model_Abstract
{

    /**
     * Entity code.
     * Can be used as part of method name for entity processing
     */
    const ENTITY = 'corra_gallery_gallery';
    const CACHE_TAG = 'corra_gallery_gallery';

    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_eventPrefix = 'corra_gallery_gallery';

    /**
     * Parameter name in event
     *
     * @var string
     */
    protected $_eventObject = 'gallery';

    /**
     * constructor
     *
     * @access public
     * @return void
     */
    public function _construct()
    {
        parent::_construct();
        $this->_init('corra_gallery/gallery');
    }

    /**
     * before save gallery
     *
     * @access protected
     * @return Corra_Gallery_Model_Gallery
     */
    protected function _beforeSave()
    {
        parent::_beforeSave();
        $now = Mage::getSingleton('core/date')->gmtDate();
        if ($this->isObjectNew()) {
            $this->setCreatedAt($now);
        }
        $this->setUpdatedAt($now);
        return $this;
    }

    /**
     * get the gallery Content
     *
     * @access public
     * @return string
     */
    public function getPostContent()
    {
        $post_content = $this->getData('post_content');
        $helper = Mage::helper('cms');
        $processor = $helper->getBlockTemplateProcessor();
        $html = $processor->filter($post_content);
        return $html;
    }

    /**
     * save gallery relation
     *
     * @access public
     * @return Corra_Gallery_Model_Gallery
     */
    protected function _afterSave()
    {
        return parent::_afterSave();
    }

    /**
     * get default values
     *
     * @access public
     * @return array
     */
    public function getDefaultValues()
    {
        $values = array();
        $values['status'] = 1;
        return $values;
    }

    /**
     * get Store View
     *
     * @access public
     * @return array
     */
    public function getStores()
    {
        if (!$this->getData('stores')) {
            return explode(',', $this->getData('stores'));
        }
        return $this->getData('stores');
    }

}
