<?php

/**
 * Gallery resource model
 *
 * @category    Corra
 * @package     Corra_Gallery
 */
class Corra_Gallery_Model_Resource_Hotspot extends Mage_Core_Model_Resource_Db_Abstract
{

    /**
     * constructor
     *
     * @access public
     */
    public function _construct()
    {
        $this->_init('corra_gallery/hotspot', 'hotspot_id');
    }

    

}
