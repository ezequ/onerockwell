<?php

/**
 * Gallery resource model
 *
 * @category    Corra
 * @package     Corra_Gallery
 */
class Corra_Gallery_Model_Resource_Gallery extends Mage_Core_Model_Resource_Db_Abstract
{

    /**
     * constructor
     *
     * @access public
     */
    public function _construct()
    {
        $this->_init('corra_gallery/gallery', 'entity_id');
    }

    /**
     * process multiple select fields
     *
     * @access protected
     * @param Mage_Core_Model_Abstract $object
     * @return Corra_Gallery_Model_Resource_Gallery
     */
    protected function _beforeSave(Mage_Core_Model_Abstract $object)
    {
        $stores = $object->getStores();
        if (is_array($stores)) {
            $object->setStores(implode(',', $stores));
        }
        return parent::_beforeSave($object);
    }

}
