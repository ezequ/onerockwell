<?php

/**
 * Gallery collection resource model
 *
 * @category    Corra
 * @package     Corra_Gallery
 */
class Corra_Gallery_Model_Resource_Hotspot_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{

    protected $_joinedFields = array();

    /**
     * constructor
     *
     * @access public
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->_init('corra_gallery/hotspot');
    }

    

}
