<?php

/**
 * Gallery Block
 *
 * @category    Corra
 * @package     Corra_Gallery
 */
class Corra_Gallery_Block_Gallery extends Mage_Core_Block_Template
{

    /**
     * Add block to cache 
     * @access public
     * @return void
     */
    protected function _construct()
    {
        $this->addData(array(
            'cache_lifetime' => 3600,
            'cache_tags' => array(Corra_Gallery_Model_Resource_Gallery_Collection::CACHE_TAG),
            'cache_key' => 'homepage_gallery_' . Mage::app()->getStore()->getStoreId(),
        ));
    }

    /**
     * Prepare Layout
     * @access public
     * @return parent
     */
    public function _prepareLayout()
    {
        return parent::_prepareLayout();
    }

    /**
     * Prepare gallery collection
     * @access public
     * @return collection
     */
    public function gallerycollection()
    {
        $storeId = Mage::app()->getStore()->getStoreId();
        $collection = Mage::getModel('corra_gallery/gallery')->getCollection();
        $collection->addFieldToFilter('status', array(array('eq' => 1)))
            // ->addFieldToFilter('stores', array(array('eq' => $storeId)))
            ->addFieldToFilter('stores', array(
                array('eq' => '0'), 
                array('eq' => $storeId), 
                array('like' => $storeId . ',%'), 
                array('like' => '%,' . $storeId . ',%'),
                array('like' => '%,' . $storeId)
                ))
            ->setOrder('image_position', 'ASC')
            ->setPageSize(5);

        return $collection;
    }
    
    public function hotspotProductCollection()
    { 
        $hotspots = Mage::getModel('corra_gallery/hotspot')->getCollection();
        foreach ($hotspots as $hotspot):
        $productSkus[] = $hotspot['sku'];        
        endforeach;
        $productCollection = Mage::getModel('catalog/product')->getCollection()
                ->addAttributeToSelect('*')
                ->addAttributeToFilter( 'sku', array( 'in' => $productSkus ) );
        $productDetails = array();
        foreach($productCollection as $product){
            $productDetails[$product->getSku()]['name'] = strlen($product->getName())< 25 ? $product->getName() : substr($product->getName(),0,24) . '...';
            $productDetails[$product->getSku()]['subtitle'] = strlen($product->getColor())< 55 ? $product->getColor() : substr($product->getColor(),0,54) . '...';
            $productDetails[$product->getSku()]['description'] = strlen($product->getShortDescription())< 196 ? $product->getShortDescription() : substr($product->getShortDescription(),0,195) . '...';
            $productDetails[$product->getSku()]['imageUrl'] = (string)Mage::helper('catalog/image')->init($product, 'small_image');
            $productDetails[$product->getSku()]['thumbImageUrl'] = (string)Mage::helper('catalog/image')->init($product, 'thumbnail')->resize(105);
            $productDetails[$product->getSku()]['productUrl'] = $this->helper('catalog/product')->getProductUrl($product);             
        }
        return json_encode($productDetails);
        
    }
    
    public function getHotspotDetails($galleryId){
        $hotspots = Mage::getModel('corra_gallery/hotspot')
                    ->getCollection()
                    ->addFieldToFilter('gallery_id',$galleryId);
        return $hotspots;
    }

}
