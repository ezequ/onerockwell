<?php

/**
 * Gallery admin block
 *
 * @category    Corra
 * @package     Corra_Gallery
 */
class Corra_Gallery_Block_Adminhtml_Gallery extends Mage_Adminhtml_Block_Widget_Grid_Container
{

    /**
     * constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        $this->_controller = 'adminhtml_gallery';
        $this->_blockGroup = 'corra_gallery';
        parent::__construct();
        $this->_headerText = Mage::helper('corra_gallery')->__('Gallery Images');
        $this->_updateButton('add', 'label', Mage::helper('corra_gallery')->__('Add Gallery Images'));
    }

}
