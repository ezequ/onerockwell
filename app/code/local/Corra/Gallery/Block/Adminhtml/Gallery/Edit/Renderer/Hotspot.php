<?php
class Corra_Gallery_Block_Adminhtml_Gallery_Edit_Renderer_Hotspot extends Mage_Adminhtml_Block_Widget
    implements Varien_Data_Form_Element_Renderer_Interface
 
{    
    /**
     * Initialize block
     */
    
    public function __construct()
    {
        $this->setTemplate('corra_gallery/hotspot.phtml');
    }

    
    

    public function render(Varien_Data_Form_Element_Abstract $element)
    {
        $this->setElement($element);
        return $this->toHtml();
    }
    

}