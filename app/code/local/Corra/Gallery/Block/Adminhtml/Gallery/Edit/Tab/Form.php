<?php

/**
 * Gallery edit form tab
 *
 * @category    Corra
 * @package     Corra_Gallery
 */
class Corra_Gallery_Block_Adminhtml_Gallery_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{

    /**
     * prepare the form
     *
     * @access protected
     * @return Corra_Gallery_Block_Adminhtml_Gallery_Edit_Tab_Form
     */
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $form->setHtmlIdPrefix('gallery_');
        $form->setFieldNameSuffix('gallery');
        $this->setForm($form);
        $fieldset = $form->addFieldset(
            'gallery_form', array('legend' => Mage::helper('corra_gallery')->__('Gallery Images'))
        );
        $fieldset->addType(
            'image', Mage::getConfig()->getBlockClassName('corra_gallery/adminhtml_gallery_helper_image')
        );
        $wysiwygConfig = Mage::getSingleton('cms/wysiwyg_config')->getConfig();

        $fieldset->addField(
            'title', 'text', array(
            'label' => Mage::helper('corra_gallery')->__('Title'),
            'name' => 'title',
            'required' => true,
            'class' => 'required-entry',
            )
        );

        $fieldset->addField(
            'desktop_image', 'image', array(
            'label' => Mage::helper('corra_gallery')->__('Desktop Image'),
            'name' => 'desktop_image',
            )
        );

        $fieldset->addField(
            'tablet_image', 'image', array(
            'label' => Mage::helper('corra_gallery')->__('Tablet Image'),
            'name' => 'tablet_image',
            )
        );

        $fieldset->addField(
            'mobile_image', 'image', array(
            'label' => Mage::helper('corra_gallery')->__('Mobile Image'),
            'name' => 'mobile_image',
            )
        );       

        $fieldset->addField(
            'link', 'text', array(
            'label' => Mage::helper('corra_gallery')->__('URL link for image'),
            'name' => 'link',
            'note' => $this->__('Please add complete URL along with http://'),
            'required' => true,
            'class' => 'required-entry',
            )
        );        
        
        $hotspot_field = $fieldset->addField('hotspot', 'text', array(
            'name'      => 'hotspot',
            'label'     => Mage::helper('corra_gallery')->__('Hotspot'),
            'required'  => false,
        ));

        $hotspot_block = $this->getLayout()
                                ->createBlock('corra_gallery/adminhtml_gallery_edit_renderer_hotspot')
                               ->setData(array(
                                    'name'      => 'hotspot',
                                    'label'     => Mage::helper('corra_gallery')->__('Hotspot'),
                                    'required'  => false,
                                ));

        $hotspot_field->setRenderer($hotspot_block);
        
        $fieldset->addField(
            'post_content', 'editor', array(
            'label' => Mage::helper('corra_gallery')->__('Content'),
            'name' => 'post_content',
            'config' => $wysiwygConfig,
            )
        );

        $fieldset->addField(
            'stores', 'multiselect', array(
            'label' => Mage::helper('corra_gallery')->__('Store View'),
            'name' => 'stores',
            'required' => true,
            'class' => 'required-entry',
            'values' => Mage::getSingleton('adminhtml/system_store')->getStoreValuesForForm(false, true),
            )
        );

        $fieldset->addField(
            'image_position', 'text', array(
            'label' => Mage::helper('corra_gallery')->__('Sort Order'),
            'name' => 'image_position',
            'required' => true,
            'class' => 'required-entry',
            )
        );
        $fieldset->addField(
            'status', 'select', array(
            'label' => Mage::helper('corra_gallery')->__('Status'),
            'name' => 'status',
            'values' => array(
                array(
                    'value' => 1,
                    'label' => Mage::helper('corra_gallery')->__('Enabled'),
                ),
                array(
                    'value' => 0,
                    'label' => Mage::helper('corra_gallery')->__('Disabled'),
                ),
            ),
            )
        );        
        $formValues = Mage::registry('current_gallery')->getDefaultValues();
        if (!is_array($formValues)) {
            $formValues = array();
        }
        if (Mage::getSingleton('adminhtml/session')->getGalleryData()) {
            $formValues = array_merge($formValues, Mage::getSingleton('adminhtml/session')->getGalleryData());
            Mage::getSingleton('adminhtml/session')->setGalleryData(null);
        } elseif (Mage::registry('current_gallery')) {
            $formValues = array_merge($formValues, Mage::registry('current_gallery')->getData());
        }
        $form->setValues($formValues);
        return parent::_prepareForm();
    }

}
