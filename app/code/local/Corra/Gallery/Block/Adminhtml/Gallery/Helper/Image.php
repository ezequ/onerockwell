<?php

/**
 * Gallery image field renderer helper
 *
 * @category    Corra
 * @package     Corra_Gallery
 */
class Corra_Gallery_Block_Adminhtml_Gallery_Helper_Image extends Varien_Data_Form_Element_Image
{

    /**
     * get the url of the image
     *
     * @access protected
     * @return string
     */
    protected function _getUrl()
    {
        $url = false;
        if ($this->getValue()) {
            $url = Mage::helper('corra_gallery/gallery_image')->getImageBaseUrl() .
                $this->getValue();
        }
        return $url;
    }

}
