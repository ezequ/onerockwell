<?php

/**
 * Gallery admin edit tabs
 *
 * @category    Corra
 * @package     Corra_Gallery
 */
class Corra_Gallery_Block_Adminhtml_Gallery_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

    /**
     * Initialize Tabs
     *
     * @access public
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('gallery_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('corra_gallery')->__('Gallery Images'));
    }

    /**
     * before render html
     *
     * @access protected
     * @return Corra_Gallery_Block_Adminhtml_Gallery_Edit_Tabs
     */
    protected function _beforeToHtml()
    {
        $this->addTab(
            'form_gallery', array(
            'label' => Mage::helper('corra_gallery')->__('Gallery Images'),
            'title' => Mage::helper('corra_gallery')->__('Gallery Images'),
            'content' => $this->getLayout()->createBlock(
                    'corra_gallery/adminhtml_gallery_edit_tab_form'
                )
                ->toHtml(),
            )
        );
        return parent::_beforeToHtml();
    }

    /**
     * Retrieve gallery entity
     *
     * @access public
     * @return Corra_Gallery_Model_Gallery
     */
    public function getGallery()
    {
        return Mage::registry('current_gallery');
    }

}
