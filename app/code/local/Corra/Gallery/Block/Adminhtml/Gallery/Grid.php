<?php

/**
 * Gallery admin grid block
 *
 * @category    Corra
 * @package     Corra_Gallery
 */
class Corra_Gallery_Block_Adminhtml_Gallery_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

    /**
     * constructor
     *
     * @access public
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('galleryGrid');
        $this->setDefaultSort('entity_id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }

    /**
     * prepare collection
     *
     * @access protected
     * @return Corra_Gallery_Block_Adminhtml_Gallery_Grid
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('corra_gallery/gallery')
            ->getCollection();

        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * prepare grid collection
     *
     * @access protected
     * @return Corra_Gallery_Block_Adminhtml_Gallery_Grid
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'entity_id', array(
            'header' => Mage::helper('corra_gallery')->__('Id'),
            'index' => 'entity_id',
            'type' => 'number'
            )
        );
        $this->addColumn(
            'title', array(
            'header' => Mage::helper('corra_gallery')->__('Title'),
            'align' => 'left',
            'index' => 'title',
            )
        );

        $this->addColumn(
            'status', array(
            'header' => Mage::helper('corra_gallery')->__('Status'),
            'index' => 'status',
            'type' => 'options',
            'options' => array(
                '1' => Mage::helper('corra_gallery')->__('Enabled'),
                '0' => Mage::helper('corra_gallery')->__('Disabled'),
            )
            )
        );
        $this->addColumn(
            'image_position', array(
            'header' => Mage::helper('corra_gallery')->__('Sort Order'),
            'index' => 'image_position',
            'type' => 'number',
            )
        );
        $this->addColumn(
            'action', array(
            'header' => Mage::helper('corra_gallery')->__('Action'),
            'width' => '100',
            'type' => 'action',
            'getter' => 'getId',
            'actions' => array(
                array(
                    'caption' => Mage::helper('corra_gallery')->__('Edit'),
                    'url' => array('base' => '*/*/edit'),
                    'field' => 'id'
                )
            ),
            'filter' => false,
            'is_system' => true,
            'sortable' => false,
            )
        );
        $this->addExportType('*/*/exportCsv', Mage::helper('corra_gallery')->__('CSV'));
        $this->addExportType('*/*/exportExcel', Mage::helper('corra_gallery')->__('Excel'));
        $this->addExportType('*/*/exportXml', Mage::helper('corra_gallery')->__('XML'));
        return parent::_prepareColumns();
    }

    /**
     * prepare mass action
     *
     * @access protected
     * @return Corra_Gallery_Block_Adminhtml_Gallery_Grid
     */
    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('entity_id');
        $this->getMassactionBlock()->setFormFieldName('gallery');
        $this->getMassactionBlock()->addItem(
            'delete', array(
            'label' => Mage::helper('corra_gallery')->__('Delete'),
            'url' => $this->getUrl('*/*/massDelete'),
            'confirm' => Mage::helper('corra_gallery')->__('Are you sure?')
            )
        );
        $this->getMassactionBlock()->addItem(
            'status', array(
            'label' => Mage::helper('corra_gallery')->__('Change status'),
            'url' => $this->getUrl('*/*/massStatus', array('_current' => true)),
            'additional' => array(
                'status' => array(
                    'name' => 'status',
                    'type' => 'select',
                    'class' => 'required-entry',
                    'label' => Mage::helper('corra_gallery')->__('Status'),
                    'values' => array(
                        '1' => Mage::helper('corra_gallery')->__('Enabled'),
                        '0' => Mage::helper('corra_gallery')->__('Disabled'),
                    )
                )
            )
            )
        );
        return $this;
    }

    /**
     * get the row url
     *
     * @access public
     * @param Corra_Gallery_Model_Gallery
     * @return string
     */
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }

    /**
     * get the grid url
     *
     * @access public
     * @return string
     */
    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current' => true));
    }

    /**
     * after collection load
     *
     * @access protected
     * @return Corra_Gallery_Block_Adminhtml_Gallery_Grid
     */
    protected function _afterLoadCollection()
    {
        $this->getCollection()->walk('afterLoad');
        parent::_afterLoadCollection();
    }

}
