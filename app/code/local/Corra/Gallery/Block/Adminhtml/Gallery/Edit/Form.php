<?php

/**
 * Gallery edit form
 *
 * @category    Corra
 * @package     Corra_Gallery
 */
class Corra_Gallery_Block_Adminhtml_Gallery_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{

    /**
     * prepare form
     *
     * @access protected
     * @return Corra_Gallery_Block_Adminhtml_Gallery_Edit_Form
     */
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form(
            array(
            'id' => 'edit_form',
            'action' => $this->getUrl(
                '*/*/save', array(
                'id' => $this->getRequest()->getParam('id')
                )
            ),
            'method' => 'post',
            'enctype' => 'multipart/form-data'
            )
        );
        $form->setUseContainer(true);
        $this->setForm($form);
        return parent::_prepareForm();
    }

}
