<?php

/**
 * Gallery admin edit form
 *
 * @category    Corra
 * @package     Corra_Gallery
 */
class Corra_Gallery_Block_Adminhtml_Gallery_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{

    /**
     * constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->_blockGroup = 'corra_gallery';
        $this->_controller = 'adminhtml_gallery';
        $this->_updateButton(
            'save', 'label', Mage::helper('corra_gallery')->__('Save Gallery')
        );
        $this->_updateButton(
            'delete', 'label', Mage::helper('corra_gallery')->__('Delete Gallery')
        );
        $this->_addButton(
            'saveandcontinue', array(
            'label' => Mage::helper('corra_gallery')->__('Save And Continue Edit'),
            'onclick' => 'saveAndContinueEdit()',
            'class' => 'save',
            ), -100
        );
        $this->_formScripts[] = "
            function saveAndContinueEdit() {
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    /**
     * get the edit form header
     *
     * @access public
     * @return string
     */
    public function getHeaderText()
    {
        if (Mage::registry('current_gallery') && Mage::registry('current_gallery')->getId()) {
            return Mage::helper('corra_gallery')->__(
                    "Edit Gallery '%s'", $this->escapeHtml(Mage::registry('current_gallery')->getTitle())
            );
        } else {
            return Mage::helper('corra_gallery')->__('Add Gallery Images');
        }
    }

}
