<?php

class Corra_LoginRegistration_Helper_Data extends Mage_Core_Helper_Abstract
{
    
    /**
     * Retrieve the login post url for modal
     * @return  string
     */
    public function getLoginPostUrl()
    {
        return $this->_getUrl('customermodal/account/loginPost');
    }

    /**
     * Retrieve the register post url for modal
     * @return  string
     */
    public function getRegisterPostUrl()
    {
        return $this->_getUrl('customermodal/account/createPost');
    }

    /**
     * Retrieve the forgot password post url for modal
     */
    public function getForgotPasswordPostUrl()
    {
        return $this->_getUrl('customermodal/account/forgotPassword');   
    }
}
