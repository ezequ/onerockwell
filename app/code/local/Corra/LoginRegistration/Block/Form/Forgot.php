<?php
class Corra_LoginRegistration_Block_Form_Forgot extends Mage_Customer_Block_Account_Forgotpassword
{
	/**
	 * To prevent calling the prepareLayout
	 */
	protected function _prepareLayout()
	{
		return $this;
	}

	/**
	 * Returns the Forgot Password Post Action Url
	 */
	public function getPostActionUrl() 
	{
		return $this->helper('corra_loginregistration')->getForgotPasswordPostUrl();
	}
}