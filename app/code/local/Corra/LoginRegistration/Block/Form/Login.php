<?php
class Corra_LoginRegistration_Block_Form_Login extends Mage_Customer_Block_Form_Login
{
	/**
	 * To prevent calling the prepareLayout
	 */
	
	protected function _prepareLayout()
	{
		return $this;
	}

	/**
	 * Returns the Login Post Action Url
	 */
	public function getPostActionUrl() 
	{
		return $this->helper('corra_loginregistration')->getLoginPostUrl();
	}
}