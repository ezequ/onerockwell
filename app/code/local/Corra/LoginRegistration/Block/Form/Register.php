<?php
class Corra_LoginRegistration_Block_Form_Register extends Mage_Customer_Block_Form_Register
{
	/**
	 * To prevent calling the prepareLayout
	 */
	protected function _prepareLayout()
	{
		return $this;
	}

	/**
	 * Returns the Register Post Action Url
	 */
	public function getPostActionUrl() 
	{
		return $this->helper('corra_loginregistration')->getRegisterPostUrl();
	}
}