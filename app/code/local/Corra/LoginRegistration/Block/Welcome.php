<?php
class Corra_LoginRegistration_Block_Welcome extends Mage_Customer_Block_Account_Dashboard_Hello
{
	/**
	 * Returns customer's first name
	 */
	public function getCustomerName()
	{
		  return Mage::getSingleton('customer/session')->getCustomer()->getFirstname();
	}
}