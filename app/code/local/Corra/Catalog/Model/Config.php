<?php

class Corra_Catalog_Model_Config extends Mage_Catalog_Model_Config
{
    public function getAttributeUsedForSortByArray()
    {
        return array_merge(
			parent::getAttributeUsedForSortByArray(),
			array('qty_ordered' => Mage::helper('catalog')->__('Top Sellers'))
		);
    }
}

