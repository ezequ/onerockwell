<?php
/**
 * Admin Side - Interior Gallery Grid Block
 *
 * @author 	 Corra
 * @package  Corra_InteriorGallery
 */
class Corra_InteriorGallery_Block_Adminhtml_Interiorgallery_Grid 
	extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('interiorgalleryGrid');
        $this->setDefaultSort('gallery_id');
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getResourceModel('interiorgallery/interiorgallery_collection');
        
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
		$this->addColumn('page_id', array(
			'header'	=> Mage::helper('interiorgallery')->__('ID'),
			'align'		=> 'right',
			'width'     => 5,
			'index'		=> 'gallery_id',
		));
		
		$this->addColumn('gallery_title', array(
			'header'	=> Mage::helper('interiorgallery')->__('Name'),
			'align'		=> 'left',
			'index'		=> 'gallery_title',
		));
		
		$this->addColumn('gallery_type', array(
            'header' => Mage::helper('interiorgallery')->__('Type'),
            'index' => 'gallery_type',
            'type'  => 'options',
            'width' => '70px',
            'options' => Mage::getModel('interiorgallery/config')->getInteriorGalleryTypesArray(),
        ));
		
		$this->addColumn('position', array(
			'header'	=> Mage::helper('interiorgallery')->__('Position'),
			'align'		=> 'left',
			'index'		=> 'position',
			'width'     => '50px',
		));
    
		$this->addColumn('gallery_status', array(
			'header'	=> Mage::helper('interiorgallery')->__('Status'),
			'index'		=> 'gallery_status',
			'type'		=> 'options',
			'options'	=> array(
				1 => Mage::helper('interiorgallery')->__('Enabled'),
				0 => Mage::helper('interiorgallery')->__('Disabled'),
			),
		));
		
		$this->addColumn('action', array(
			'header' => Mage::helper('interiorgallery')->__('Action'),
			'type'      => 'action',
			'getter'     => 'getId',
			'actions'   => array(array(
				'caption' => Mage::helper('interiorgallery')->__('Edit'),
				'url'     => array(
				'base'=>'*/interiorgallery/edit',
				),
				'field'   => 'gallery_id'
			)),
			'filter'    => false,
			'sortable'  => false,
			'align' 	=> 'center',
		));

        return parent::_prepareColumns();
    }

    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('gallery_id' => $row->getId()));
    }
}
