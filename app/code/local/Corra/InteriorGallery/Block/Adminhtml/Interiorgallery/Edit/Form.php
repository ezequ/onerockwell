<?php
/**
 * Admin Side - Interior Gallery Edit Form Block
 *
 * @author 	 Corra
 * @package  Corra_InteriorGallery
 */
class Corra_InteriorGallery_Block_Adminhtml_Interiorgallery_Edit_Form 
	extends Mage_Adminhtml_Block_Widget_Form
{
	protected function _prepareForm()
  	{
    	$form = new Varien_Data_Form(
			array(
				'id' => 'edit_form',
				'action' => $this->getUrl('*/*/save', array('gallery_id' => $this->getRequest()->getParam('id'))),
				'method' => 'post',
				'enctype' => 'multipart/form-data'
			)
		);
      	$form->setUseContainer(true);
      	$this->setForm($form);
      	return parent::_prepareForm();
  	}
}