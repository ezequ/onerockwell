<?php
/**
 * Admin Side - Interior Gallery Add/Edit Page 'General Information' Tab
 *
 * @author 	 Corra
 * @package  Corra_InteriorGallery
 */
class Corra_InteriorGallery_Block_Adminhtml_Interiorgallery_Edit_Tab_Info 
	extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $model = Mage::registry('interiorgallery');
        
        $form = new Varien_Data_Form();
        
        $fieldset = $form->addFieldset('info_fieldset', array('legend'=>Mage::helper('interiorgallery')->__('General Information')));
		
		$fieldset->addType(
            'image', Mage::getConfig()->getBlockClassName('interiorgallery/adminhtml_interiorgallery_helper_image')
        );

        if ($model->getGalleryId()) 
        	$fieldset->addField('gallery_id', 'hidden', array('name' => 'gallery_id'));
        
        $fieldset->addField('gallery_title', 'text', array(
            'name'  => 'gallery_title',
            'label' => Mage::helper('interiorgallery')->__('Name'),
            'id'    => 'gallery_title',
            'title' => Mage::helper('interiorgallery')->__('Name'),
            'required' => true,
        )); 
		
		$fieldset->addField('gallery_type', 'select', array(
			'name' => 'gallery_type',
			'title' => Mage::helper('interiorgallery')->__('Type'),
			'label' => Mage::helper('interiorgallery')->__('Type'),
			'values' => Mage::getModel('interiorgallery/config')->getInteriorGalleryTypes(),
			'required' => true,
		));  
		
		$field = $fieldset->addField('gallery_url_key', 'text', array(
			'name'     =>  'gallery_url_key',
			'label'    =>  Mage::helper('interiorgallery')->__('URL Key'),
			'title'    =>  Mage::helper('interiorgallery')->__('URL Key'),
			'required' =>  true,
		));
		
		$fieldset->addField('gallery_thumbimage', 'image', array(
			'name' 	=> 'gallery_thumbimage',
			'label' => Mage::helper('interiorgallery')->__('Thumbnail Image'),
			'title' => Mage::helper('interiorgallery')->__('Thumbnail Image'),
		));
		
		$field = $fieldset->addField('position', 'text', array(
			'name'     =>  'position',
			'label'    =>  Mage::helper('interiorgallery')->__('Position'),
			'title'    =>  Mage::helper('interiorgallery')->__('Position'),
			'required' =>  true,
		));
		
		$fieldset->addField('gallery_status', 'select', array(
			'name' => 'gallery_status',
			'title' => Mage::helper('interiorgallery')->__('Enabled'),
			'label' => Mage::helper('interiorgallery')->__('Enabled'),
			'values' => Mage::getModel('adminhtml/system_config_source_yesno')->toOptionArray(),
		));   
        
        $data = $model->getData();
        $form->setValues($data);
        $this->setForm($form);
        
        return parent::_prepareForm();
    }
}
