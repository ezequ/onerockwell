<?php
/**
 * Admin Side - Interior Gallery Add/Edit Page Left Menu
 *
 * @author 	 Corra
 * @package  Corra_InteriorGallery
 */
class Corra_InteriorGallery_Block_Adminhtml_Interiorgallery_Edit_Tabs 
	extends Mage_Adminhtml_Block_Widget_Tabs
{

    public function __construct()
    {
        parent::__construct();
        $this->setId('interiorgallery_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('interiorgallery')->__('Interior Gallery Information'));
    }

    protected function _beforeToHtml()
    {
    	$tabs = array(
			'info' => 'General Information',
			'images' => 'Images',
			'meta' => 'Meta Data',
		);
		
		foreach($tabs as $alias => $label) {
			$this->addTab($alias, array(
				'label' => $this->__($label),
				'title' => $this->__($label),
				'content' => $this->getLayout()->createBlock('interiorgallery/adminhtml_interiorgallery_edit_tab_' . $alias)->toHtml(),
			));
		}
		return parent::_beforeToHtml();
    }
}
