<?php
/**
 * Admin Side - Interior Gallery Add/Edit Page 'Meta Data' Tab
 *
 * @author 	 Corra
 * @package  Corra_InteriorGallery
 */
class Corra_InteriorGallery_Block_Adminhtml_Interiorgallery_Edit_Tab_Meta extends Mage_Adminhtml_Block_Widget_Form
{
	/**
	 * Add the meta fields to the form
	 *
	 * @return $this
	 */
	protected function _prepareForm()
	{
		$model = Mage::registry('interiorgallery');
		
		$form = new Varien_Data_Form();
		
		$fieldset = $form->addFieldset('meta_fieldset', array('legend'=>Mage::helper('adminhtml')->__('Meta Data')));
		
		$fieldset->addField('gallery_meta_title', 'text', array(
			'name' => 'gallery_meta_title',
			'label' => Mage::helper('interiorgallery')->__('Title'),
			'title' =>  Mage::helper('interiorgallery')->__('Title'),
		));
		
		$fieldset->addField('gallery_meta_description', 'editor', array(
			'name' => 'gallery_meta_description',
			'label' => Mage::helper('interiorgallery')->__('Description'),
			'title' => Mage::helper('interiorgallery')->__('Description'),
			'style' => 'width:98%; height:110px;',
		));
		
		$fieldset->addField('gallery_meta_keywords', 'editor', array(
			'name' => 'gallery_meta_keywords',
			'label' => Mage::helper('interiorgallery')->__('Keywords'),
			'title' => Mage::helper('interiorgallery')->__('Keywords'),
			'style' => 'width:98%; height:110px;',
		));
		
		$data = $model->getData();
        $form->setValues($data);
        $this->setForm($form);
        
        return parent::_prepareForm();
	}
}
