<?php
/**
 * Admin Side - Interior Gallery Edit Block
 *
 * @author 	 Corra
 * @package  Corra_InteriorGallery
 */
class Corra_InteriorGallery_Block_Adminhtml_Interiorgallery_Edit 
	extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
    	parent::__construct();
                 
        $this->_objectId = 'gallery_id';
        $this->_blockGroup = 'interiorgallery';
        $this->_controller = 'adminhtml_interiorgallery';
		$this->_addButton('save_and_edit_button', array(
			'label'     => Mage::helper('interiorgallery')->__('Save and Continue Edit'),
			'onclick'   => 'editForm.submit(\''.$this->getSaveAndContinueUrl().'\')',
			'class' => 'save'
		));
      	
        return parent::__construct();
    }
	
	/**
	 * Retrieve the URL used for the save and continue link
	 * This is the same URL with the back parameter added
	 *
	 * @return string
	 */
	public function getSaveAndContinueUrl()
	{
		return $this->getUrl('*/*/save', array(
			'_current'   => true,
			'back'       => 'edit',
		));
	}
	
	/**
     * get the edit form header
     *
     * @access public
     * @return string
     */
    public function getHeaderText()
    {
        if (Mage::registry('interiorgallery') && Mage::registry('interiorgallery')->getId()) {
            return Mage::helper('interiorgallery')->__(
                    "Edit '%s'", $this->escapeHtml(Mage::registry('interiorgallery')->getGalleryTitle())
            );
        } else {
            return Mage::helper('interiorgallery')->__('New Interior Gallery');
        }
    }
}