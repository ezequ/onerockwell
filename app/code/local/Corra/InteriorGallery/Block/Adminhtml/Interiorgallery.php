<?php
/**
 * Admin Side - Interior Gallery list Block
 *
 * @author 	 Corra
 * @package  Corra_InteriorGallery
 */
class Corra_InteriorGallery_Block_Adminhtml_Interiorgallery 
	extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
    	$this->_blockGroup = 'interiorgallery';
        $this->_controller = 'adminhtml_interiorgallery';
        
        $this->_headerText = Mage::helper('interiorgallery')->__('Manage Interior Gallery');
        $this->_addButtonLabel = Mage::helper('interiorgallery')->__('Add Interior Gallery');
        parent::__construct();
    }
}
