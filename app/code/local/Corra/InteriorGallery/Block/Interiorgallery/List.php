<?php
/**
 * Front End - Interior Gallery List Block
 *
 * @author 	 Corra
 * @package  Corra_InteriorGallery
 */
class Corra_InteriorGallery_Block_Interiorgallery_List extends Mage_Core_Block_Template
{
	/**
	 * Retrieve Gallery List
	 *
	 * @return array $galleryList
	 */
	public function getInteriorGalleryList()
	{
		$galleryList = Mage::registry('interiorgallery_list');
		if ($galleryList) {
			return $galleryList;
		} else {
			$galleryList = array(
				'room' 		=> '',
				'materials' => '',
			);
			return $galleryList;
		}
	}
}
