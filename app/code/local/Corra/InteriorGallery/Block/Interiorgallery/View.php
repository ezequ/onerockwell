<?php
/**
 * Front End - Interior Gallery View (Gallery detail page) Block
 *
 * @author 	 Corra
 * @package  Corra_InteriorGallery
 */
class Corra_InteriorGallery_Block_Interiorgallery_View extends Mage_Core_Block_Template
{
	/**
	 * Retrieve Gallery Details
	 *
	 * @return Corra_Brandpages_Model_Group
	 */
	public function getInteriorGalleryDetail()
	{
		$gallery = Mage::registry('interiorgallery_view');
		$imgCollection = $gallery['images'];
		$imgCount = $imgCollection->getSize();
		if ($gallery) {
			$galleryId = $gallery['gallery']->getId();
			$galleryType = $gallery['gallery']->getGalleryType();
			$galleryCollection = Mage::getResourceModel('interiorgallery/interiorgallery_collection')
				->addFieldToFilter('gallery_status', 1)
				->addFieldToFilter('gallery_type', array ('eq' => $galleryType))
				->addFieldToFilter('gallery_id', array ('neq' => $galleryId));
			if ($imgCount > 0) {
				$imageArr = array();
				$count = 0;
				foreach ($imgCollection as $imgObject) {
					$count++;	
					$prodCollection = '';
					if ($imgObject->getSkus()) {
						$skusArr = explode(",",  $imgObject->getSkus());
						$productsArr = '';
						if (count($skusArr) > 0) {
							$skuArray = array();
							foreach ($skusArr as $key => $sku) {
								$skuArray[] = trim($sku);
							}
							$prodCollection =  Mage::getResourceModel('catalog/product_collection')
								->addAttributeToSelect('*')
								->addAttributeToFilter('sku', array('in' => $skuArray));
							if ($prodCollection->getSize() > 0) {
								foreach($prodCollection as $prodObj) {
									$productsArr[] = $prodObj;
								}
							}
						}
					}
					$imageArr[] = array(
						'img_id'   => $imgObject->getId(),
						'count'    => $count,
						'name'     => $imgObject->getImgName(),
						'products' => $productsArr,
					);
				}
			} else {
				$imageArr = '';
			}
			
			$returnArr = array(
				'title'	 => $gallery['gallery']->getGalleryTitle(),
				'images' => array(
					'total' => $imgCount,
					'data'  => $imageArr,
				),
				'other_galleries' => $galleryCollection,
			);
		} else {
			$returnArr = array(
				'title'	 		  => '',
				'images' 		  => array(
					'total' => 0,
					'data'  => '',
				),
				'other_galleries' => '',
			);
		}
		return $returnArr;
	}
	
	public function getPriceHtml($product)
    {
        $this->setTemplate('catalog/product/price.phtml');
        $this->setProduct($product);
        return $this->toHtml();
    }
	
	/**
     * Retrieve attribute instance by name, id or config node
     *
     * If attribute is not found false is returned
     *
     * @param string|integer|Mage_Core_Model_Config_Element $attribute
     * @return Mage_Eav_Model_Entity_Attribute_Abstract || false
     */
    public function getProductAttribute($attribute)
    {
        return $this->getProduct()->getResource()->getAttribute($attribute);
    }
}
