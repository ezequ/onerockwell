<?php 
/**
 * Front End - Interior Gallery Controller
 *
 * @author 	 Corra
 * @package  Corra_InteriorGallery
 */
class Corra_InteriorGallery_InteriorgalleryController extends Mage_Core_Controller_Front_Action
{
	/**
	 * Display the Interior Gallery List Page
	 *
	 * @return void
	 */
	public function listAction()
    {
		$galleryList = $this->_initGalleryList();
		$this->loadLayout();
		$rootBlock = $this->getLayout()->getBlock('root');
		if ($rootBlock) {
			$rootBlock->addBodyClass('interiorgallery-list');
		}
		$headBlock = $this->getLayout()->getBlock('head');
		if ($headBlock) {
			$headBlock->setTitle('Gallery');
		}
		$breadBlock = $this->getLayout()->getBlock('breadcrumbs');
		if ($breadBlock) {
			$breadBlock->addCrumb('home', array('label' => $this->__('Home'), 'title' => $this->__('Home'), 'link' => Mage::getUrl()));
			$breadBlock->addCrumb('interiorgallery-list', array('label' => $this->__('Gallery'), 'title' => $this->__('Gallery')));
		}
		$this->renderLayout();
    }

    /**
     * Fetch Gallery List Collection
     *
     * @return array $galleryArr
     */
    protected function _initGalleryList()
    {
        $roomList = Mage::getResourceModel('interiorgallery/interiorgallery_collection')
			->addFieldToFilter('gallery_status', 1)
			->addFieldToFilter('gallery_type', array ('eq' => 'room'))
			->setOrder('position', 'asc')
			->load();
		
		$materialsList = Mage::getResourceModel('interiorgallery/interiorgallery_collection')
			->addFieldToFilter('gallery_status', 1)
			->addFieldToFilter('gallery_type', array ('eq' => 'materials'))
			->setOrder('position', 'asc')
			->load();
			
		$galleryArr = array(
			'room'      => $roomList,
			'materials' => $materialsList,
		);
		
		Mage::register('interiorgallery_list', $galleryArr);
        return $galleryArr;
    }
	
	/**
	 * Display the Interior Gallery Image Detail Page
	 *
	 * @return void
	 */
	public function viewAction()
    {
		$gallery = $this->_initGalleryDetailPage();
		if ($gallery === false) {
			return $this->_forward('noRoute');
		}
				
		$galleryPage = $gallery['gallery'];
		
		$this->loadLayout();
		
		$rootBlock = $this->getLayout()->getBlock('root');
		if ($rootBlock) {
			$rootBlock->addBodyClass('interiorgallery-view');
		}
		if ($headBlock = $this->getLayout()->getBlock('head')) {
			if ($title = $galleryPage->getGalleryMetaTitle()) {
				$headBlock->setTitle($title);
			} else {
				$headBlock->setTitle($galleryPage->getGalleryTitle());
			}

			if ($description = $galleryPage->getGalleryMetaDescription()) {
				$headBlock->setDescription($description);
			}
			
			if ($keywords = $galleryPage->getGalleryMetaKeywords()) {
				$headBlock->setKeywords($keywords);
			}
			
			$headBlock->addItem('link_rel',  Mage::getBaseUrl() . 'gallery/' . $galleryPage->getGalleryUrlKey(), 'rel="canonical"');
		}
		$breadBlock = $this->getLayout()->getBlock('breadcrumbs');
		if ($breadBlock) {
			$breadBlock->addCrumb('home', array('label' => $this->__('Home'), 'title' => $this->__('Home'), 'link' => Mage::getUrl()));
			$breadBlock->addCrumb('gallery', array('label' => $this->__('Gallery'), 'title' => $this->__('Home'), 'link' => Mage::getBaseUrl() . 'gallery'));
			$breadBlock->addCrumb('interiorgallery-view', array('label' => $galleryPage->getGalleryTitle(), 'title' => $galleryPage->getGalleryTitle()));
		}
		$this->renderLayout();
	}

    /**
     * Fetch Gallery Details
     *
     * @return bool false|array $galleryArr
     */
    protected function _initGalleryDetailPage()
    {
		$galleryId = (int) $this->getRequest()->getParam('id', false);
        		
		$galleryCollection = Mage::getResourceModel('interiorgallery/interiorgallery_collection')
			->addFieldToFilter('gallery_status', 1)
			->addFieldToFilter('gallery_id', array ('eq' => $galleryId))
			->getFirstItem();
				
		$imgCollection = '';	
		if ($galleryCollection) {
			$imgCollection = Mage::getResourceModel('interiorgallery/img_collection')
				->addFieldToFilter('gallery_id', array ('eq' => $galleryId))
				->setOrder('img_order', 'asc')
				->load();
		} else {
			return false;
		}
			
		$galleryArr = array(
			'gallery' => $galleryCollection,
			'images'  => $imgCollection,
		);
		
		Mage::register('interiorgallery_view', $galleryArr);
        return $galleryArr;
    }
}
