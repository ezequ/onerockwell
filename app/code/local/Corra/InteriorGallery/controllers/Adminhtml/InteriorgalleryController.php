<?php
/**
 * Admin Side - Interior Gallery Controller
 *
 * @author 	 Corra
 * @package  Corra_InteriorGallery
 */
class Corra_InteriorGallery_Adminhtml_InteriorgalleryController 
	extends Mage_Adminhtml_Controller_Action
{
	/**
     * Interior Gallery list
     */
	public function indexAction()
    {
		$this->loadLayout();
	   	$this->_title($this->__("Interior Gallery"));
		$this->_setActiveMenu('interiorgallery/interiorgallery');
	   	$this->renderLayout();
    }
    
    protected function _initGallery()
    {
		$id = $this->getRequest()->getParam('gallery_id');
        $model = Mage::getModel('interiorgallery/interiorgallery');
        $model_img = Mage::getModel('interiorgallery/img')->getCollection();
        
        if ($id) {
            $model->load($id);
            $model_img->addFieldToFilter('gallery_id', array('in'=>array($id)));
            if (! $model->getGalleryId()) {
                Mage::getSingleton('adminhtml/session')->addError($this->__('This Gallery no longer exists'));
                 $this->_redirect('*/interiorgallery');
                return;
            }
        }
        
        $data = Mage::getSingleton('adminhtml/session')->getInteriorgalleryData(true);
        if (!empty($data)) {
            $model->setData($data);
       }
       Mage::register('interiorgallery', $model);
       Mage::register('interiorgallery_img', $model_img);
	}
    
   /**
     * Edit Interior Gallery Page
     */
    public function editAction()
    {
		$this->_initGallery();
		$this->loadLayout();
	    $this->_title($this->__("Interior Gallery"));
		$this->_setActiveMenu('interiorgallery/interiorgallery');
	    $this->renderLayout();
    }
    
 	/**
     * Add New Interior Gallery Page
     */
	public function newAction()
    { 
        return $this->_forward('edit');
    }
    
    /**
     * Save Interior Gallery Page
     */
	public function saveAction()
    {
		$id = $this->getRequest()->getParam('gallery_id');
        $data = $this->getRequest()->getPost();
        $datax = $this->getRequest()->getPost('interiorgallery_images');
        
        $datay = Zend_Json::decode($datax);
        $_photos_info = array();
        $_photos_info = Zend_Json::decode($datax);
        
    	if ($data) {
            $model = new Corra_InteriorGallery_Model_Interiorgallery();
			if (!$model->isUrlKeyValid($data['gallery_url_key'], $id)) {
				Mage::getSingleton('adminhtml/session')->addError(Mage::helper('interiorgallery')->__('Duplicate URL Key \'%s\'', $data['gallery_url_key']));
				if ($id) {
                    $this->_redirect('*/*/edit', array('gallery_id' => $id));
                    return;
                } else {
					$this->_redirect('*/*/new');
					return;
				}
			}
			$currentDate = Varien_Date::now();
        	$currentTimestamp = Varien_Date::toTimestamp($currentDate);
			if ($id) {
				$data['update_time'] = $currentTimestamp;
			} else {
				$data['created_time'] = $currentTimestamp;
			}
			Mage::log(var_export($data, true), NULL, 'Deffy.log', true);
            $model->setData($data);
            
			$_conn = Mage::getSingleton('core/resource')->getConnection('core_write');      

            try {
            	$this->_handleImageUpload($model, 'gallery_thumbimage');
            	$model->save();

            	if(!empty($_photos_info)) {
            		foreach($_photos_info as $_photo_info) {
            			
            			//Do updade if we have gallery id (menaing photo is already saved)
    					if(isset($_photo_info['gallery_id'])) {
    						
    						$data = array(
	    						"img_name" => str_replace(".tmp", "", $_photo_info['file']),
	    						"skus" => $_photo_info['label'],
	    						"gallery_id" => $_photo_info['gallery_id'],
	    						"img_order" => $_photo_info['position'],
	    						"img_name" => str_replace(".tmp", "", $_photo_info['file'])
    						);
    						
    						$where = array("img_id = ".(int)$_photo_info['value_id']);
    						$_conn->update('corra_interiorgallery_images', $data, $where);

    						if(isset($_photo_info['removed']) and $_photo_info['removed'] == 1) {
	    						$_conn->delete('corra_interiorgallery_images', 'img_id = '.(int)$_photo_info['value_id']);

	    					}
    					} else {
    						$_lookup = $_conn->fetchAll("SELECT * FROM corra_interiorgallery_images WHERE img_name = ?", $_photo_info['file']);
    						
    						if(empty($_lookup)) {
    							
    							$_conn->insert('corra_interiorgallery_images', array(
    								'img_name' => str_replace(".tmp", "", $_photo_info['file']),
 	   								'skus' => $_photo_info['label'],
    								'gallery_id' => (int)$model->getId(),
    								'img_order' => $_photo_info['position']
    								
    							));
    						}
    					}
            		}	
            	}
				Mage::getSingleton('adminhtml/session')->addSuccess($this->__('Gallery was successfully saved'));
                Mage::getSingleton('adminhtml/session')->getInteriorgalleryData(false);
				if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('gallery_id' => $model->getId()));
                    return;
                }
                $this->_redirect('*/interiorgallery');
                return;
            } catch (Exception $e) {
              Mage::getSingleton('adminhtml/session')->addError(Mage::helper('interiorgallery')->__('Some Error Occurred: %s', $e->getMessage()));
			  $this->_redirect('*/interiorgallery');
              return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('interiorgallery')->__('Unable to find Gallery to save'));
        $this->_redirect('*/interiorgallery');
    }  

    /**
     * Delete Interior Gallery Page
     */
	public function deleteAction()
    {
    	$id = $this->getRequest()->getParam('gallery_id');
        if ($id) {
            try {
                $model = Mage::getModel('interiorgallery/interiorgallery');
                $model->setId($id);
                $model->delete();
                Mage::getSingleton('adminhtml/session')->addSuccess($this->__('Gallery was successfully deleted'));
                 $this->_redirect('*/interiorgallery');
                return;
            }catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('gallery_id' => $id));
                return;
            }
        }
		Mage::getSingleton('adminhtml/session')->addError($this->__('Unable to find a Gallery to delete'));
        $this->_redirect('*/interiorgallery');
    }

    public function addAction()
    {
    	$id = $this->getRequest()->getParam('gallery_id');
        $data = $this->getRequest()->getPost();
        
    	if ($data) {
            $model = new Corra_InteriorGallery_Model_Interiorgallery();

			$model_img = new Corra_InteriorGallery_Model_Img();

            try {
                $model->save();
                
				Mage::getSingleton('adminhtml/session')->addSuccess($this->__('Gallery was successfully added'));
                Mage::getSingleton('adminhtml/session')->getInteriorgalleryData(false);
				$this->_redirect('*/interiorgallery');
                return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setInteriorgalleryData($data);
                $this->_redirect('*/*/edit', array('gallery_id' => $model->getGalleryId()));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('interiorgallery')->__('Unable to find Gallery to save'));
       $this->_redirect('*/interiorgallery');
    }  
	
    public function uploadAction()
    {
		$result = array();
        try {
            $uploader = new Varien_File_Uploader('image');
            $uploader->setAllowedExtensions(array('jpg','jpeg','gif','png'));
            $uploader->setAllowRenameFiles(true);
            $uploader->setFilesDispersion(true);
            $result = $uploader->save(
                Mage::getSingleton('catalog/product_media_config')->getBaseTmpMediaPath()
            );

            $result['url'] = Mage::getSingleton('catalog/product_media_config')->getTmpMediaUrl($result['file']);
            $result['file'] = $result['file'] . '.tmp';
            $result['cookie'] = array(
                'name'     => session_name(),
                'value'    => $this->_getSession()->getSessionId(),
                'lifetime' => $this->_getSession()->getCookieLifetime(),
                'path'     => $this->_getSession()->getCookiePath(),
                'domain'   => $this->_getSession()->getCookieDomain()
            );
        } catch (Exception $e) {
            $result = array('error'=>$e->getMessage(), 'errorcode'=>$e->getCode());
        }

        $this->getResponse()->setBody(Zend_Json::encode($result));
    }
    
 	public function imageAction()
    {
		$result = array();
        try {
            $uploader = new Corra_InteriorGallery_Media_Uploader('image');
			$uploader->setAllowedExtensions(array('jpg','jpeg','gif','png'));
			$uploader->setAllowRenameFiles(true);
            $uploader->setFilesDispersion(true);
            $result = $uploader->save(
                Mage::getSingleton('media/config')->getBaseTmpMediaPath()
            );
			$result['url'] = Mage::getSingleton('media/config')->getTmpMediaUrl($result['file']);
            $result['file'] = $result['file'] . '.tmp';
            $result['cookie'] = array(
                'name'     => session_name(),
                'value'    => $this->_getSession()->getSessionId(),
                'lifetime' => $this->_getSession()->getCookieLifetime(),
                'path'     => $this->_getSession()->getCookiePath(),
                'domain'   => $this->_getSession()->getCookieDomain()
            );
			Mage::log(var_export($result, true), NULL, 'Upload.log', true);
        } catch (Exception $e) {
			$result = array('error'=>$e->getMessage(), 'errorcode'=>$e->getCode());
        }
		
        $this->getResponse()->setBody(Zend_Json::encode($result));
    }
	
	protected function _handleImageUpload(Corra_InteriorGallery_Model_Interiorgallery $page, $field)
	{
		$data = $page->getData($field);

		if (isset($data['value'])) {
			$page->setData($field, $data['value']);
		}

		if (isset($data['delete']) && $data['delete'] == '1') {
			$page->setData($field, '');
		}

		if ($filename = Mage::helper('interiorgallery/image')->uploadImage($field)) {
			$page->setData($field, $filename);
		}
	}
}