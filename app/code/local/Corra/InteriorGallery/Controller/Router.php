<?php
/**
 * URL Router
 *
 * @author 	 Corra
 * @package  Corra_InteriorGallery
 */
class Corra_InteriorGallery_Controller_Router extends Mage_Core_Controller_Varien_Router_Abstract
{
	/**
	 * Cache for request object
	 *
	 * @var Zend_Controller_Request_Http
	 */
	protected $_request = null;

	/**
	 * Initialize Controller Router
	 *
	 * @param Varien_Event_Observer $observer
	*/
	public function initControllerRouters(Varien_Event_Observer $observer)
	{
		$observer->getEvent()->getFront()->addRouter('interiorgallery', $this);
	}

	/**
	 * Get the request object
	 *
	 * @return Zend_Controller_Request_Http
	 */
	public function getRequest()
	{
		return $this->_request;
	}

    /**
     * Validate and Match Cms Page and modify request
     *
     * @param Zend_Controller_Request_Http $request
     * @return bool
     */
    public function match(Zend_Controller_Request_Http $request)
    { 
		$this->_request = $request;
		        
		if (($requestUri = $this->_preparePathInfo($request->getPathInfo())) === false) {
			return false;
		}
		if ($this->_match($requestUri) !== false) {
			$request->setAlias(
				Mage_Core_Model_Url_Rewrite::REWRITE_REQUEST_PATH_ALIAS, 
				$requestUri
			);
			return true;
		}
		return false;
	}
	
	/**
	 * Match the request against all enabled splash routes
	 *
	 * @return bool
	 */
	protected function _match(&$requestUri)
	{
		$isDoubleBarrel = strpos($requestUri, '/') !== false;
		
		if ($isDoubleBarrel) {
			if (substr_count($requestUri, '/') > 1) {
				return false;
			}
			
			list($groupUrlKey, $pageUrlKey) = explode('/', $requestUri);
			return $this->_loadInteriorGalleryDetailPage($pageUrlKey);
		}
		if ($requestUri == 'gallery') {
			return $this->_loadInteriorGalleryListPage($requestUri);
		}
		if ($this->_loadInteriorGalleryDetailPage($requestUri)) {
			return true;
		} 
		return false;
    }

	/**
	 * Prepare the path info variable
	 *
	 * @param string $pathInfo
	 * @return false|string
	 */
	protected function _preparePathInfo($pathInfo)
	{
		$requestUri = trim($pathInfo, '/');
		return $requestUri;
	}
		
	/**
	 * Load Interior Gallery Detail Page
	 *
	 * @param string $urlKey
	 * @return bool
	 */
	protected function _loadInteriorGalleryDetailPage($urlKey)
	{
		if ($urlKey) {
			$pages = Mage::getResourceModel('interiorgallery/interiorgallery_collection')
				->addFieldToFilter('gallery_url_key', $urlKey)
				->addFieldToFilter('gallery_status', 1)
				->load();
		} else {
			return false;
		}
		
		if (count($pages) === 0) {
			return false;
		}
		
		foreach($pages as $object) {
			$page = $object;
			break;
		}
		
		if (!$page) {
			return false;
		}
		
		$this->getRequest()->setModuleName('interiorgallery')
			->setControllerName('interiorgallery')
			->setActionName('view')
			->setParam('id', $page->getId());
		return true;
	}
	
	/**
	 * Load Interior Gallery List Page
	 *
	 * @param string $urlKey
	 * @return bool
	 */
	protected function _loadInteriorGalleryListPage($urlKey)
	{
		$this->getRequest()->setModuleName('interiorgallery')
			->setControllerName('interiorgallery')
			->setActionName('list');
		return true;
	}
}
