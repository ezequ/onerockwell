<?php

$installer = $this;

$installer->startSetup();

$installer->run("
    
DROP TABLE IF EXISTS {$this->getTable('corra_interiorgallery')};
CREATE TABLE {$this->getTable('corra_interiorgallery')} (
  `gallery_id` int(11) NOT NULL auto_increment,
  `gallery_title` varchar(255) default NULL,
  `gallery_type` varchar(255) default NULL,
  `gallery_thumbimage` varchar(255) default NULL,
  `gallery_url_key` varchar(255) NOT NULL default '',
  `gallery_meta_title` text,
  `gallery_meta_keywords` text,
  `gallery_meta_description` text,
  `gallery_status` smallint(6) NOT NULL default '0',
  `created_time` timestamp NULL,
  `update_time` timestamp NULL,
  PRIMARY KEY  (`gallery_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Corra interiorgallery' ;

DROP TABLE IF EXISTS {$this->getTable('corra_interiorgallery_images')};
CREATE TABLE {$this->getTable('corra_interiorgallery_images')} (
  `img_id` int(11) NOT NULL auto_increment,
  `img_name` varchar(255) default NULL,
  `skus` varchar(255) default NULL,
  `gallery_id` int(11) default NULL,
  `img_order` int(5) default NULL,
  PRIMARY KEY  (`img_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Corra interiorgallery images' ;

");

$installer->endSetup();