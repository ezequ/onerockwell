<?php
$installer = $this;
$installer->startSetup();
$installer->getConnection()->addColumn($installer->getTable('corra_interiorgallery'), 'position', 'int(5) NULL after `gallery_url_key`');
$installer->endSetup();