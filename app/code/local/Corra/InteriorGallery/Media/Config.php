<?php

class Corra_InteriorGallery_Media_Config extends Mage_Catalog_Model_Product_Media_Config
{

    public function getBaseMediaPath()
    {
        return Mage::getBaseDir('media') . DS . 'interiorgallery';
    }

    public function getBaseMediaUrl()
    {
        return Mage::getBaseUrl('media') . 'interiorgallery';
    }

    public function getBaseTmpMediaPath()
    {
        return Mage::getBaseDir('media') . DS . 'interiorgallery';
    }

    public function getBaseTmpMediaUrl()
    {
        return Mage::getBaseUrl('media') . 'interiorgallery';
    }
     
}