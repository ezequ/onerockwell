<?php
class Corra_InteriorGallery_Model_Config extends Mage_Core_Model_Config_Base
{
	/**
     * Gallery Type Options getter
     *
     * @return array
     */
    public function getInteriorGalleryTypes()
    {
        return array(
            array('value' => 'room', 'label'=>Mage::helper('adminhtml')->__('Room')),
            array('value' => 'materials', 'label'=>Mage::helper('adminhtml')->__('Materials')),
        );
    }
	
	/**
     * Gallery Type Options in "key-value" format
     *
     * @return array
     */
    public function getInteriorGalleryTypesArray()
    {
        return array(
			'room'      => Mage::helper('adminhtml')->__('Room'),
            'materials' => Mage::helper('adminhtml')->__('Materials'),
        );
    }
}
