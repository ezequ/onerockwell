<?php
/**
 * Interior Gallery - corra_interiorgallery table Resource Model
 *
 * @author 	 Corra
 * @package  Corra_InteriorGallery
 */
class Corra_InteriorGallery_Model_Resource_Interiorgallery 
	extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct()
    {
        $this->_init('interiorgallery/interiorgallery', 'gallery_id');
    }
}
