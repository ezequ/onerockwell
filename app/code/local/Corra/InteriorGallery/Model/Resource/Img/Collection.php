<?php
/**
 * Interior Gallery - corra_interiorgallery_images table Resource Collection Model
 *
 * @author 	 Corra
 * @package  Corra_InteriorGallery
 */
class Corra_InteriorGallery_Model_Resource_Img_Collection 
	extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    protected function _construct()
    {
        $this->_init('interiorgallery/img');
    }
}
