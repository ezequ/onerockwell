<?php
class Corra_InteriorGallery_Model_Interiorgallery extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('interiorgallery/interiorgallery');
    }
	
	/**
	 * Checking if Url Key of Interior Gallery is already exists or not in the following table
	 * `corra_interiorgallery` 
     *
	 * @param string $urlKey
	 * @param integer $galleryId
	 * @return boolean true|false
	 */
	public function isUrlKeyValid($urlKey, $galleryId='')
	{
		if($galleryId) {
			$galleryCollection = $this->getCollection()
				->addFieldToFilter('gallery_url_key', array('eq' => $urlKey))
				->addFieldToFilter('gallery_id', array('neq' => $galleryId));
		} else {
			$galleryCollection = $this->getCollection()
				->addFieldToFilter('gallery_url_key', array('eq' => $urlKey));
		}
		if($galleryCollection->getSize() > 0) {
			return false;
		} 
		return true;
	}
}
