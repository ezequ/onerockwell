<?php
class Corra_Topsellers_Model_Observer
{
    public function getTopsellers(Varien_Event_Observer $observer)
    {
        $order = $observer->getEvent()->getOrder();
        $items = $order->getAllVisibleItems();
        foreach($items as $item):
        $product_id = $item->getProductId();
        $product = Mage::getModel('catalog/product')->load($product_id);
        $qty_ordered = $product->getTopSellers()+$item->getData('qty_ordered');
        $store_id = 0;
        $action = Mage::getModel('catalog/resource_product_action');
        $action->updateAttributes(array($product_id), array(
            'top_sellers' => $qty_ordered
        ), $store_id);
        endforeach;        
    }
}