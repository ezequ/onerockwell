<?php
/**
 * @category    Corra
 * @package     Corra_Brandpages 
 */
class Corra_Brandpages_Model_Brandpage extends Corra_Brandpages_Model_Abstract
{
    protected function _construct(){

       $this->_init("brandpages/brandpage");

    }
    
    /**
	 * Retrieve the name of the splash page
	 * If display name isn't set, option value label will be returned
	 *
	 * @return string
	 */
	public function getName()
	{
		return $this->getDisplayName() ? $this->getDisplayName() : $this->getFrontendLabel();
	}

	/**
	 * Determine whether the model is active
	 *
	 * @return bool
	 */
	public function isActive()
	{
		return (($group = Mage::registry('brand_page')) !== null)
			&& $group->getId() === $this->getId();
	}
        
    /**
	 * Retrieve the URL for the splash group
	 * If cannot find rewrite, return system URL
	 *
	 * @return string
	 */
	public function getUrl()
	{
		/*if (!$this->hasUrl()) {
			$this->setUrl(
				$this->_getUrl($this->getUrlKey() . '.html')
			);
		}*/
		
		if (!$this->hasUrl()) {
			$this->setUrl(
				$this->_getUrl($this->getUrlKey())
			);
		}
		
		return $this->_getData('url');
	}
	
	/**
	 * Retrieve the attribute model for the page
	 *
	 * @return Mage_Eav_Model_Entity_Attribute
	 */
	public function getAttributeModel()
	{
		if (!$this->hasAttributeModel()) {
			$this->setAttributeModel($this->getResource()->getAttributeModel($this));
		}
		
		return $this->getData('attribute_model');
	}
	
	/**
	 * Retrieve a collection of products associated with the splash page
	 *
	 * @return Mage_Catalog_Model_Resource_Eav_Mysql4_Product_Collection
	 */
	public function getBrandPages()
	{
		return $this->getResource()->getBrandPages($this);
	}
        
	public function getAttributeCode(){
		return "web_collection_name";
	}
        
    /**
	 * Retrieve a collection of products associated with the splash page
	 *
	 * @return Mage_Catalog_Model_Resource_Eav_Mysql4_Product_Collection
	 */
	public function getProductCollection()
	{
		if (!$this->hasProductCollection()) {
			$this->setProductCollection($this->getResource()->getProductCollection($this));
		}
		
		return $this->getData('product_collection');
	}
        
    public function getCategory()
	{
		if (($category = parent::getCategory()) !== false) {
			return $category;
		}
	}
}
	 