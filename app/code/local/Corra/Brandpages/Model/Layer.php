<?php
/**
 * @category    Corra
 * @package     Corra_Brandpages
 */
class Corra_Brandpages_Model_Layer extends Mage_Catalog_Model_Layer 
{
	/**
	 * Retrieve the current category
	 *
	 * @return Mage_Catalog_Model_Category
	 */
	public function getCurrentCategory()
	{	
		if (!$this->hasCurrentCategory()) {
			if ($this->getBrandPage() && $category = $this->getBrandPage()->getCategory()) {
				$this->setData('current_category', $category);
			}
		}

		return parent::getCurrentCategory();
	}

	/**
	 * Retrieve the brand page
	 * We add an array to children_categories so that it can act as a category
	 *
	 * @return false|Corra_Brandpages_Model_Brandpage
	 */
	public function getBrandPage()
	{
		if ($page = Mage::registry('brand_page')) {
			return $page;
		}
		
		return false;
	}

	/**
	 * Get the state key for caching
	 *
	 * @return string
	 */
	 public function getStateKey()
	 {
		if ($this->getBrandPage() && $this->_stateKey === null) {
			$this->_stateKey = 'STORE_'.Mage::app()->getStore()->getId()
				. '_BRAND_' . $this->getBrandPage()->getId()
				. '_CUSTGROUP_' . Mage::getSingleton('customer/session')->getCustomerGroupId();
			}

		return $this->_stateKey;
	}

	/**
	 * Get default tags for current layer state
	 *
	 * @param   array $additionalTags
	 * @return  array
	*/
	public function getStateTags(array $additionalTags = array())
	{
		if ($this->getBrandPage()) {
			$additionalTags = array_merge($additionalTags, array(
				Mage_Catalog_Model_Category::CACHE_TAG.$this->getBrandPage()->getId()
			));
		}
		
		return $additionalTags;
	}

	/**
	 * Retrieve the product collection for the Splash Page
	 *
	 * @return
	 */

	 public function getProductCollection()
	 {
	 	if ($this->getBrandPage()) {
			$key = 'brand_' . $this->getBrandPage()->getId();
	
			if (isset($this->_productCollections[$key])) {
				$collection = $this->_productCollections[$key];
			}
			else {
				$collection = $this->getBrandPage()->getProductCollection();                        
				$this->prepareProductCollection($collection);
				$this->_productCollections[$key] = $collection;
			}
		} else {
			if (isset($this->_productCollections[$this->getCurrentCategory()->getId()])) {
				$collection = $this->_productCollections[$this->getCurrentCategory()->getId()];
			} else {
				$collection = $this->getCurrentCategory()->getProductCollection();
				$this->prepareProductCollection($collection);
				$this->_productCollections[$this->getCurrentCategory()->getId()] = $collection;
			}
		}
		return $collection;
	}

	/**
	 * Stop the splash page attribute from dsplaying in the filter options
	 *
	 * @param   Mage_Catalog_Model_Resource_Eav_Mysql4_Attribute_Collection $collection
	 * @return  Mage_Catalog_Model_Resource_Eav_Mysql4_Attribute_Collection
     */
	protected function _prepareAttributeCollection($collection)
	{
		parent::_prepareAttributeCollection($collection);
		
		if ($this->getBrandPage() && $splash = $this->getBrandPage()) {
			$collection->addFieldToFilter('attribute_code', array('neq' => $splash->getAttributeCode()));
		}
		
		return $collection;
	}
}
