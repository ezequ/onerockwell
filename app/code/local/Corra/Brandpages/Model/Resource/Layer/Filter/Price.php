<?php
/**
 * Catalog Layer Price Filter resource model
 *
 * @category    Corra
 * @package     Corra_Brandpages 
 */
class Corra_Brandpages_Model_Resource_Layer_Filter_Price extends Mage_Catalog_Model_Resource_Layer_Filter_Price
{
    /**
     * Retrieve array with products counts per price range
     *
     * @param Mage_Catalog_Model_Layer_Filter_Price $filter
     * @param int $range
     * @return array
     */
    public function getCount($filter, $range)
    {
        $select = $this->_getSelect($filter);
        $priceExpression = $this->_getFullPriceExpression($filter, $select);

        /**
         * Check and set correct variable values to prevent SQL-injections
         */
        $range = floatval($range);
        if ($range == 0) {
            $range = 1;
        }
        $countExpr = new Zend_Db_Expr('COUNT(*)');
        $rangeExpr = new Zend_Db_Expr("FLOOR(({$priceExpression}) / {$range}) + 1");

        $select->columns(array(
            'range' => $rangeExpr,
            'count' => $countExpr
        ));
        $select->group($rangeExpr)->order("$rangeExpr ASC");
        if(!Mage::registry("current_category")) {
             $newSelect = "SELECT `range`,COUNT(count) count FROM (".$select.") src GROUP BY `range`";
         } else {
            $newSelect = $select;
         }
        return $this->_getReadAdapter()->fetchPairs($newSelect);
    }
}
