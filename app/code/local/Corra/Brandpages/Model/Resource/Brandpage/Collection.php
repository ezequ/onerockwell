<?php
/**
 * @category    Corra
 * @package     Corra_Brandpages 
 */

class Corra_Brandpages_Model_Resource_Brandpage_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
	public function _construct()
	{
		$this->_init("brandpages/brandpage");
	}
			
	/**
	* Order the groups by name
	*
	* @return $this
	*/
  	public function addOrderByName()
   	{
		$this->getSelect()->order('main_table.display_name ASC');
		$this->getSelect()->order('_attribute_table.frontend_label ASC');
 	    return $this;
    }
   
   /**
	* Add filter by store
	*
	* @param int|Mage_Core_Model_Store $store
	* @param bool $withAdmin
	* @return Mage_Cms_Model_Resource_Page_Collection
   */
   public function addStoreFilter($store, $withAdmin = true)
   {
	   if ($store instanceof Mage_Core_Model_Store) {
			   $store = array($store->getId());
	   }

	   if (!is_array($store)) {
			   $store = array($store);
	   }

	   if ($withAdmin) {
			   $store[] = Mage_Core_Model_App::ADMIN_STORE_ID;
	   }

	   return $this->addFieldtoFilter('main_table.store_id', array('in' => $store));
   }
}
 