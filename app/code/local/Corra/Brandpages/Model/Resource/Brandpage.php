<?php
/**
 * @category    Corra
 * @package     Corra_Brandpages 
 */
class Corra_Brandpages_Model_Resource_Brandpage extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct()
    {
        $this->_init("brandpages/brandpage", "page_id");
    }
    
    /**
	 * Retrieve select object for load object data
	 * This gets the default select, plus the attribute id and code
	 *
	 * @param   string $field
	 * @param   mixed $value
	 * @return  Zend_Db_Select
	*/
	protected function _getLoadSelect($field, $value, $object)
	{
		$select = parent::_getLoadSelect($field, $value, $object)
			->join(array('_option_table' => $this->getTable('eav/attribute_option')), 
				'`_option_table`.`option_id` = '.$this->getTable('brandpages/brandpage').'.`option_id`', '')
			->join(array('_attribute_table' => $this->getTable('eav/attribute')), 
				'`_attribute_table`.`attribute_id`=`_option_table`.`attribute_id`', 
				array('attribute_id', 'attribute_code', 'frontend_label'));		
		return $select;
	}
        
    /**
	 * Retrieve the name of the unique field
	 *
	 * @return string
	 */
	public function getUniqueFieldName()
	{
		return 'option_id';	
	}
        
    /**
	 * Retrieve a collection of products associated with the splash page
	 *
	 * @return Mage_Catalog_Model_Resource_Eav_Resource_Product_Collection
	 */	
	public function getProductCollection(Corra_Brandpages_Model_Brandpage $page)
	{	
        $attributeId = Mage::helper('brandpages')->getManufacturerId();
                
		/*$collection = Mage::getResourceModel('catalog/product_collection')
			->setStoreId($page->getStoreId())
			->addAttributeToFilter('status', 1);*/
		$collection = Mage::getResourceModel('catalog/product_collection')
			->setStoreId($page->getStoreId());

		$alias = $page->getAttributeCode().'_index';

		$collection->getSelect()
			->join(
				array($alias => $this->getTable('catalog/product_index_eav')),
				"`{$alias}`.`entity_id` = `e`.`entity_id`"
				. $this->_getReadAdapter()->quoteInto(" AND `{$alias}`.`attribute_id` = ? ", $attributeId)
				. $this->_getReadAdapter()->quoteInto(" AND `{$alias}`.`store_id` = ? ", $page->getStoreId())
				. $this->_getReadAdapter()->quoteInto(" AND `{$alias}`.`value` = ?", $page->getOptionId()),
				''
			);
                         
		if (!Mage::getStoreConfigFlag('cataloginventory/options/show_out_of_stock', $page->getStoreId())) {
			Mage::getSingleton('cataloginventory/stock')->addInStockFilterToCollection($collection);
		}

		return $collection;
	}
}