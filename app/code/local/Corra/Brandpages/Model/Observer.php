<?php
/**
 * @category    Corra
 * @package     Corra_Brandpages 
 */
class Corra_Brandpages_Model_Observer
{
    /**
     * Inject links into the top navigation
     * @param Varien_Event_Observer $observer
     * @return $this
     */
    public function injectTopmenuLinksObserver(Varien_Event_Observer $observer)
    {
        $pages = Mage::getModel('brandpages/brandpage')->getCollection()
            ->addStoreFilter(Mage::app()->getStore()->getId());
        $this->_injectLinks($pages, $observer->getEvent()->getMenu());
        return $this;
    }

    /**
     * Inject links into the top navigation
     *
     * @param Mage_Core_Model_Resource_Db_Collection_Abstract $items
     * @param Varien_Data_Tree_Node $parentNode
     * @return bool
     */
    protected function _injectLinks($items, $parentNode)
    {
        if (!$parentNode) {
            return false;
        }
        $parent = $this->getBrandAttrDetails();
        if ($parent['is_active']) {
            $parentNode->setIsActive(true);
            $buffer = $parentNode;
            while ($buffer->getParent()) {
                $buffer = $buffer->getParent();
                $buffer->setIsActive(true);
            }
        }
        $itemNode = new Varien_Data_Tree_Node($parent, 'id', $parentNode->getTree(), $parentNode);
        $parentNode->addChild($itemNode);
        foreach ($items as $item) {
            if (!$item->canIncludeInMenu()) {
                continue;
            }
            $data = array(
                'name' => $item->getName(),
                'id' => $item->getMenuNodeId(),
                'url' => $item->getUrl(),
                'is_active' => $item->isActive(),
            );
            if ($data['is_active']) {
                $itemNode->setIsActive(true);
                $buffer = $itemNode;
                while ($buffer->getParent()) {
                    $buffer = $buffer->getParent();
                    $buffer->setIsActive(true);
                }
            }
            $newNode = new Varien_Data_Tree_Node($data, 'id', $itemNode->getTree(), $itemNode);
            $itemNode->addChild($newNode);
        }
        return true;
    }

    /**
     * Get Get Brand attribute details
     */
    public function getBrandAttrDetails()
    {
        $attribute_code = "web_collection_name";
        $attribute_details = Mage::getSingleton("eav/config")->getAttribute('catalog_product', $attribute_code);
        $attribute = $attribute_details->getData();
        $label = $attribute['frontend_label'];
       // $url = $this->_getUrl("brands") . ".html";
	    $url = $this->_getUrl("brands");
        $menu_node_id = 'brand-group0';
        $is_active = 1;
        $data = array(
            'name' => $label,
            'id' => $menu_node_id,
            'url' => $url,
            'is_active' => $is_active,
        );
        return $data;
    }

    protected function _getUrl($uri)
    {
        $storeId = Mage::app()->getStore()->getId();
        $url = Mage::getUrl('', array(
                '_direct' => $uri,
                '_secure' => false,
                '_nosid' => true,
                '_store' => $storeId,
        ));
        if ($storeId === 0 || Mage::getStoreConfigFlag('web/seo/use_rewrites')) {
            $url = str_replace('/' . basename($_SERVER['SCRIPT_FILENAME']), '', $url);
        }
        if (Mage::getStoreConfigFlag('web/url/use_store') && $storeId === 0) {
            $url = str_replace('/admin/', '/', $url);
        }
        return $url;
    }

    /**
     * Remove the e.visibility where part
     *
     * @param Varien_Event_Observer $observer
     * @return $this
     */
    public function prepareCatalogPriceSelectObserver(Varien_Event_Observer $observer)
    {
        $where = $observer->getEvent()
            ->getSelect()
            ->getPart(Zend_Db_Select::WHERE);
        foreach ($where as $key => $value) {
            if (strpos($value, 'e.visibility') !== false) {
                unset($where[$key]);
                break;
            }
        }
        $observer->getEvent()
            ->getSelect()
            ->setPart(Zend_Db_Select::WHERE, $where);
        return $this;
    }

    public function createBrandPage(Varien_Event_Observer $observer)
    {
        $attribute = $observer->getEvent()->getAttribute()->getAttributeCode();
        if ($attribute == "web_collection_name") {
            $currentCollection = Mage::getModel('brandpages/brandpage')->getCollection();
            $i = 0;
            foreach ($currentCollection as $current) {
                $currentPages[$i] = $current->getOptionId();
                $i++;
            }
            $attributeDetails = Mage::getSingleton("eav/config")
                ->getAttribute("catalog_product", 'web_collection_name');
            $options = $attributeDetails->getSource()->getAllOptions(false);
            foreach ($options as $option) {
                $key = $option["value"];
                $label = $option["label"];
                $attributeList[$key] = $label;
            }
            $page = Mage::getModel('brandpages/brandpage');
            foreach ($attributeList as $key => $value) {
                if (!in_array($key, $currentPages)) {
                    $name = $value;
                    $page->setDisplayName($name);
                    $urlkey = $this->formatUrlKey($name);
                    $page->setUrlKey($urlkey);
                    $page->setIsEnabled(1);
                    $page->setOptionId($key);
                    $page->save();
                    $page->unsetData();
                }
            }
        }
    }

    public function formatUrlKey($str)
    {
        $str = Mage::helper('catalog/product_url')->format($str);
        $urlKey = preg_replace('#[^0-9a-z]+#i', '-', $str);
        $urlKey = strtolower($urlKey);
        $urlKey = trim($urlKey, '-');
        return $urlKey;
    }

}
