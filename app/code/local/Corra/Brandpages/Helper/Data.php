<?php
/**
 * @category    Corra
 * @package     Corra_Brandpages 
 */
class Corra_Brandpages_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * Disable the MageWorx_SeoSuite rewrites for the layered navigation
     *
     * @return $this
     */
    public function clearLayerRewrites()
    {
        
    }

    public function getManufacturerId()
    {
        $attributeId = Mage::getResourceModel('eav/entity_attribute')->getIdByCode('catalog_product', 'web_collection_name');
        return $attributeId;
    }

    public function getBrandAttrDetails()
    {
        $attribute_code = "web_collection_name";
        $attribute_details = Mage::getSingleton("eav/config")->getAttribute('catalog_product', $attribute_code);
        $attribute = $attribute_details->getData();

        return $attribute;
    }

    public function getSortedBrands()
    {
        $allBrands = Mage::getModel('brandpages/brandpage')->getCollection();
        $brandArr = array();
        foreach (range('a', 'z') as $letter) {
            $i = 0;
            foreach ($allBrands as $brands) {
                $check = strtolower(substr($brands->getDisplayName(), 0, 1));
                if ($check === $letter) {
                    $brandArr[$letter][$i] = $brands;
                    $bArr[$letter][$i] = $brands->getDisplayName();
                    $i++;
                }
            }
            if ($i == 0) {
                $brandArr[$letter][$i] = "NA";
            }
        }
        $i = 0;
        foreach (range(1, 9) as $number) {
            foreach ($allBrands as $brands) {
                $check = strtolower(substr($brands->getDisplayName(), 0, 1));
                if ($check == $number) {
                    $brandArr['num'][$i] = $brands;
                    $bArr['num'][$i] = $brands->getDisplayName();
                    $i++;
                }
            }
        }
        if ($i == 0) {
            $brandArr['num'][$i] = "NA";
        }
        foreach ($brandArr as $key => $brands) {
            $sortArr = @$bArr[$key];
            if (is_array($sortArr)) {
                sort($sortArr, SORT_REGULAR);
                $i = 0;
                foreach ($sortArr as $k => $val) {
                    foreach ($brands as $brand) {
                        if (is_object($brand) && $val == $brand->getDisplayName()) {
                            $finalArr[$key][$i] = $brand;
                            $i++;
                        }
                    }
                }
                if ($i == 0) {
                    $finalArr[$key] = "NA";
                }
            } else {
                $finalArr[$key] = "NA";
            }
        }

        return $finalArr;
    }

    public function getUrl($uri)
    {
        $storeId = (int) Mage::app()->getStore(true)->getId();
        $urlKey = $this->formatUrlKey($uri);
        /*$url = Mage::getUrl('', array(
                '_direct' => $urlKey . '.html',
                '_secure' => false,
                '_nosid' => true,
                '_store' => $storeId,
        ));*/
		$url = Mage::getUrl('', array(
                '_direct' => $urlKey,
                '_secure' => false,
                '_nosid' => true,
                '_store' => $storeId,
        ));
        if ($storeId === 0 || Mage::getStoreConfigFlag('web/seo/use_rewrites')) {
            $url = str_replace('/' . basename($_SERVER['SCRIPT_FILENAME']), '', $url);
        }
        if (Mage::getStoreConfigFlag('web/url/use_store') && $storeId === 0) {
            $url = str_replace('/admin/', '/', $url);
        }

        return $url;
    }

    public function formatUrlKey($str)
    {
        $str = Mage::helper('catalog/product_url')->format($str);
        $urlKey = preg_replace('#[^0-9a-z]+#i', '-', $str);
        $urlKey = strtolower($urlKey);
        $urlKey = trim($urlKey, '-');
        return $urlKey;
    }

}
