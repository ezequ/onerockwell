<?php
/**
 * @category    Corra
 * @package     Corra_Brandpages 
 */
class Corra_Brandpages_Controller_Router extends Mage_Core_Controller_Varien_Router_Abstract
{
	/**
	 * Cache for request object
	 *
	 * @var Zend_Controller_Request_Http
	 */
	protected $_request = null;

	/**
	 * Initialize Controller Router
	 *
	 * @param Varien_Event_Observer $observer
	*/
	public function initControllerRouters(Varien_Event_Observer $observer)
	{
		$observer->getEvent()->getFront()->addRouter('brandpages', $this);
	}

	/**
	 * Get the request object
	 *
	 * @return Zend_Controller_Request_Http
	 */
	public function getRequest()
	{
		return $this->_request;
	}

    /**
     * Validate and Match Cms Page and modify request
     *
     * @param Zend_Controller_Request_Http $request
     * @return bool
     */
    public function match(Zend_Controller_Request_Http $request)
    { 
        $this->_request = $request;
                
		if (($requestUri = $this->_preparePathInfo($request->getPathInfo())) === false) {
			return false;
		}

		if ($this->_match($requestUri) !== false) {
			
		    $request->setAlias(
				Mage_Core_Model_Url_Rewrite::REWRITE_REQUEST_PATH_ALIAS, 
				$requestUri
			);

			Mage::helper('brandpages')->clearLayerRewrites();
			
			return true;
		}
		
		return false;
	}
	
	/**
	 * Match the request against all enabled splash routes
	 *
	 * @return bool
	 */
	protected function _match(&$requestUri)
	{
		$isDoubleBarrel = strpos($requestUri, '/') !== false;
		
		if ($isDoubleBarrel) {
			if (substr_count($requestUri, '/') > 1) {
				return false;
			}
			
			list($groupUrlKey, $pageUrlKey) = explode('/', $requestUri);
			return $this->_loadSplashPage($pageUrlKey, $groupUrlKey);
		}
		if ($this->_loadSplashPage($requestUri)) {
			return true;
		} else{
			return false;
		}
	}

	/**
	 * Prepare the path info variable
	 *
	 * @param string $pathInfo
	 * @return false|string
	 */
	protected function _preparePathInfo($pathInfo)
	{
		$requestUri = trim($pathInfo, '/');
		return $requestUri;
	}
		
	/**
	 * Load a splash page by it's URL key
	 * If the group URL key is present, this must match
	 *
	 * @param string $pageUrlKey
	 * @param string $groupUrlKey = null
	 * @return bool
	 */
	protected function _loadSplashPage($pageUrlKey, $groupUrlKey = null)
	{
		$pages = Mage::getResourceModel('brandpages/brandpage_collection')
			->addStoreFilter(Mage::app()->getStore()->getId())
			->addFieldToFilter('url_key', $pageUrlKey)
			->load();

		if (count($pages) === 0) {
			return false;
		}

		$page = false;

		foreach($pages as $object) {
			if (!$object->getIsEnabled()) {
				continue;
			}
                        
			$page = $object;

			break;
		}
		
		if (!$page) {
			return false;
		}
                
        Mage::register('brand_page', $page);
		
		$this->getRequest()->setModuleName('brands')
			->setControllerName('brandpage')
			->setActionName('view')
			->setParam('id', $page->getId());
					
		return true;
	}
}
