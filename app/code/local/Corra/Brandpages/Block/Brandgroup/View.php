<?php
/**
 * @category    Corra
 * @package     Corra_Brandpages
 */
class Corra_Brandpages_Block_Brandgroup_View extends Mage_Core_Block_Template
{
	/**
	 * Splash page collection
	 *
	 * @var Fishpig_AttributeSplash_Model_Mysql4_Page_Collection
	 */
	protected $_splashPages = null;
        
	/**
	 * Retrieve the splash group
	 *
	 * @return Corra_Brandpages_Model_Group
	 */
	public function getBrandGroup()
	{
		if (!$this->hasSplashGroup()) {
			return Mage::registry('brand_group');
		}
		
		return $this->_getData('brand_group');
	}
	
	/**
	 * Retrieve the splash page collection
	 *
	 * @return Corra_Brandpages_Model_Resource_Page_Collection
	 */
	public function getSplashPages()
	{
		if (is_null($this->_splashPages)) {
			$this->_splashPages = $this->getBrandGroup();
		}
		
		return $this->_splashPages;
	}
	
	/**
	 * Check if category display mode is "Products Only"
	 *
	 * @return bool
	*/
	public function isProductMode()
	{
		return true;
	}
	
	/**
	 * Check if category display mode is "Static Block and Products"
	 *
	 * @return bool
	*/
	public function isMixedMode()
	{
		return $this->getSplashGroup()->getDisplayMode()==Mage_Catalog_Model_Category::DM_MIXED;
	}

	/**
	 * Determine whether it is content mode (Static Block)
	 *
	 * @return bool
	 */
	public function isContentMode()
	{
		return $this->getSplashGroup()->getDisplayMode()==Mage_Catalog_Model_Category::DM_PAGE;
	}

	/**
	 * Retrieves the HTML for the CMS block
	 *
	 * @return string
	 */
	public function getCmsBlockHtml()
	{
		if (!$this->_getData('cms_block_html')) {
			$html = $this->getLayout()->createBlock('cms/block')
				->setBlockId($this->getSplashGroup()->getCmsBlock())->toHtml();

			$this->setCmsBlockHtml($html);
		}
		
		return $this->_getData('cms_block_html');
	}
	
	/**
	 * Retrieve the amount of columns for grid view
	 *
	 * @return int
	 */
	public function getColumnCount()
	{
		return $this->hasColumnCount() 
			? $this->_getData('column_count') 
			: Mage::getStoreConfig('attributeSplash/group/column_count');
	}
	
	public function getThumbnailUrl($page)
	{
		return $this->helper('brandpages/image')->init($page, 'thumbnail')
			->keepFrame($page->thumbnailShouldKeepFrame())
			->resize($page->getThumbnailWidth(), $page->getThumbnailHeight());
	}
}
