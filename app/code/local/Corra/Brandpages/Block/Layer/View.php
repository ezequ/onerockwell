<?php
/**
 * @category    Corra
 * @package     Corra_Brandpages 
 */
class Corra_Brandpages_Block_Layer_View extends Mage_Catalog_Block_Layer_View
{
	
    protected function _construct()
    {
        parent::_construct();

        /* To get picked up by Mage_Catalog_Product_List::getLayer() */
        Mage::register('current_layer', $this->getLayer(), true);
    }
    
    /**
	 * Returns the layer object for the brandpages model
	 *
	 * @return Corra_Brandpages_Model_Layer
	 */
	public function getLayer()
	{
		return Mage::getSingleton('brandpages/layer');
	}
	
	/**
	 * Ensure the default Magento blocks are used
	 *
	 * @return $this
	 */
   /* protected function _initBlocks()
    {
    	parent::_initBlocks();

		$this->_stateBlockName = 'Mage_Catalog_Block_Layer_State';
		$this->_categoryBlockName = 'Mage_Catalog_Block_Layer_Filter_Category';
		$this->_attributeFilterBlockName = 'Mage_Catalog_Block_Layer_Attribute';
		$this->_priceFilterBlockName = 'Mage_Catalog_Block_Layer_Price';
		$this->_decimalFilterBlockName = 'Mage_Catalog_Block_Layer_Filter_Decimal';

        return $this;
    }*/
}
