<?php
/**
 * @category    Corra
 * @package     Corra_Brandpages 
 */
class Corra_Brandpages_Block_Adminhtml_Brandpages_Edit_Tab_Images extends Corra_Brandpages_Block_Adminhtml_Brandpages_Edit_Tab_Abstract
{
	/**
	 * Retrieve Additional Element Types
	 *
	 * @return array
	*/
	protected function _getAdditionalElementTypes()
	{
		return array(
			'image' => Mage::getConfig()->getBlockClassName('brandpages/adminhtml_brandpages_helper_image')
		);
	}

	/**
	 * Setup the form fields
	 *
	 * @return $this
	 */
	protected function _prepareForm()
	{
		parent::_prepareForm();
		
		$fieldset = $this->getForm()
			->addFieldset('brand_image', array('legend'=> $this->__('Images')));

		$this->_addElementTypes($fieldset);
		
		$fieldset->addField('image', 'image', array(
			'name' 	=> 'image',
			'label' => $this->__('Logo'),
			'title' => $this->__('Logo'),
		));
		
		$this->getForm()->setValues($this->_getFormData());

		return $this;
	}
}
