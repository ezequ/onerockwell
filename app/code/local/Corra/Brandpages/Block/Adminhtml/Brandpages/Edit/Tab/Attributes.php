<?php
/**
 * @category    Corra
 * @package     Corra_Brandpages 
 */
class Corra_Brandpages_Block_Adminhtml_Brandpages_Edit_Tab_Attributes extends Corra_Brandpages_Block_Adminhtml_Brandpages_Edit_Tab_Abstract
{
	/**
	 * Add the design elements to the form
	 *
	 * @return $this
	 */
	protected function _prepareForm()
	{
		parent::_prepareForm();

		$fieldset = $this->getForm()->addFieldset('brand_attributes', array(
			'legend'=> $this->helper('adminhtml')->__('Attributes'),
			'class' => 'fieldset-wide',
		));

		$page = Mage::registry('brand_page');
                
               $fieldset->addField('attribute_id', 'hidden', array(
                        'name' 		=> 'attribute_id',                        
                ));
                
		$fieldset->addField('option_id', 'select', array(
			'name' => 'option_id',
			'label' => $this->__('Option'),
			'title' => $this->__('Option'),
			'values' => $this->_getPageOptionValues($page),
			'required' => true,
			'disabled' => !is_null($page),
		));
		$this->getForm()->setValues($this->_getFormData());
		
		return $this;
	}
	
	/**
	 * Retrieve the option values for the page
	 * 
	 * @param Corra_Brandpages_Model_Brandpage $page = null
	 * @return array
	 */
	protected function _getPageOptionValues($page = null)
	{
		if (is_null($page)) {
			$attributeId = Mage::getResourceModel('eav/entity_attribute')->getIdByCode('catalog_product','web_collection_name');
                        $collection =Mage::getResourceModel('eav/entity_attribute_option_collection')
                                       ->setPositionOrder('asc')
                                       ->setAttributeFilter($attributeId)
                                       ->setStoreFilter(0)
                                       ->load();
                        $option = $collection->toOptionArray();
                        return $option;
		}
		
		$option = Mage::getResourceModel('eav/entity_attribute_option_collection')
			->setStoreFilter($page->getStoreId())
			->addFieldToFilter('main_table.option_id', $page->getOptionId())
			->setPageSize(1)
			->load()
			->getFirstItem();
		
		if (!$option->getId()) {
			return array();
		}
		
		return array(array(
			'value' => $page->getOptionId(),
			'label' => $option->getValue(),
		));
	}
        
	protected function _getManufacturerId()
	{
		$attributeId = Mage::getResourceModel('eav/entity_attribute')->getIdByCode('catalog_product','web_collection_name');
		return $attributeId;
	}
}
