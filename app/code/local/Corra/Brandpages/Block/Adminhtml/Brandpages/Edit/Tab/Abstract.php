<?php
/**
 * @category    Corra
 * @package     Corra_Brandpages 
 */
abstract class Corra_Brandpages_Block_Adminhtml_Brandpages_Edit_Tab_Abstract extends Mage_Adminhtml_Block_Widget_Form
{
	/**
	 * Generate the form object
	 *
	 * @return $this
	 */
	protected function _prepareForm()
	{
		$form = new Varien_Data_Form();

        $form->setHtmlIdPrefix('brand');
        $form->setFieldNameSuffix('brand');
        
		$this->setForm($form);

		return parent::_prepareForm();
	}
	
	/**
	 * Retrieve the data used for the form
	 *
	 * @return array
	 */
	protected function _getFormData()
	{
		if ($page = Mage::registry('brand_page')) {
			return $page->getData();
		}
		return array('is_enabled' => 1, 'store_ids' => array(0), 'include_in_menu' => 1,'attribute_id' => $this->_getManufacturerId());
	}
        
	protected function _getManufacturerId()
	{
		$attributeId = Mage::getResourceModel('eav/entity_attribute')->getIdByCode('catalog_product','web_collection_name');
		return $attributeId;
	}
}
