<?php
/**
 * @category    Corra
 * @package     Corra_Brandpages 
 */
class Corra_Brandpages_Block_Adminhtml_Brandpages_Edit_Tab_Meta extends Corra_Brandpages_Block_Adminhtml_Brandpages_Edit_Tab_Abstract
{
	/**
	 * Add the meta fields to the form
	 *
	 * @return $this
	 */
	protected function _prepareForm()
	{
		parent::_prepareForm();
		
		$fieldset = $this->getForm()->addFieldset('brandpage_meta', array(
			'legend'=> $this->helper('adminhtml')->__('Meta Data'),
			'class' => 'fieldset-wide',
		));


		$fieldset->addField('page_title', 'text', array(
			'name' => 'page_title',
			'label' => $this->__('Page Title'),
			'title' => $this->__('Page Title'),
		));
		
		$fieldset->addField('meta_description', 'editor', array(
			'name' => 'meta_description',
			'label' => $this->__('Description'),
			'title' => $this->__('Description'),
			'style' => 'width:98%; height:110px;',
		));
		
		$fieldset->addField('meta_keywords', 'editor', array(
			'name' => 'meta_keywords',
			'label' => $this->__('Keywords'),
			'title' => $this->__('Keywords'),
			'style' => 'width:98%; height:110px;',
		));
		
		$this->getForm()->setValues($this->_getFormData());
		
		return $this;
	}
}
