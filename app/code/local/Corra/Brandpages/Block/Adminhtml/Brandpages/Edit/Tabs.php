<?php
/**
 * @category    Corra
 * @package     Corra_Brandpages 
 */
class Corra_Brandpages_Block_Adminhtml_Brandpages_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
	/**
	 * Init the tabs block
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
		$this->setId('brand_page_tabs');
		$this->setDestElementId('edit_form');
		$this->setTitle($this->__('Collection Page Information'));
	}
	
	/**
	 * Add tabs to the block
	 *
	 * @return $this
	 */
	protected function _beforeToHtml()
	{
		$tabs = array(
			'general' => 'Page Information',
			'attributes' => 'Attributes',
			'content' => 'Content',
			'images' => 'Images',
			'meta' => 'Meta Data',
		);
		
		foreach($tabs as $alias => $label) {
			$this->addTab($alias, array(
				'label' => $this->__($label),
				'title' => $this->__($label),
				'content' => $this->getLayout()->createBlock('brandpages/adminhtml_brandpages_edit_tab_' . $alias)->toHtml(),
			));
		}
		
		Mage::dispatchEvent('attributesplash_adminhtml_page_edit_tabs', array('tabs' => $this));
		
		return parent::_beforeToHtml();
	}
}
