<?php
/**
 * @category    Corra
 * @package     Corra_Brandpages
 */
class Corra_Brandpages_Block_Adminhtml_Brandpages_Edit_Tab_General extends Corra_Brandpages_Block_Adminhtml_Brandpages_Edit_Tab_Abstract
{
	/**
	 * Setup the form fields
	 *
	 * @return $this
	 */
	protected function _prepareForm()
	{
		parent::_prepareForm();

		$fieldset = $this->getForm()
			->addFieldset('brand_page_information', array(
				'legend'=> $this->__('Page Information')
			));
		
		$fieldset->addField('display_name', 'text', array(
			'name' 		=> 'display_name',
			'label' 	=> $this->__('Name'),
			'title' 	=> $this->__('Name'),
			'required'	=> true,
			'class'		=> 'required-entry',
		));

		$field = $fieldset->addField('url_key', 'text', array(
			'name' => 'url_key',
			'label' => $this->__('URL Key'),
			'title' => $this->__('URL Key'),
		));
		
		if ($page = Mage::registry('brand_page')) {
			$fieldset->addField('attribute_id', 'hidden', array(
				'name' 		=> 'attribute_id',
				'value' => $page->getAttributeId(),
			));
			
			$fieldset->addField('option_id', 'hidden', array(
				'name' 		=> 'option_id',
				'value' => $page->getOptionId(),
			));
		}
                
		if (!Mage::app()->isSingleStoreMode()) {
			$field = $fieldset->addField('store_ids', 'multiselect', array(
				'name' => 'store_ids[]',
				'label' => Mage::helper('cms')->__('Store View'),
				'title' => Mage::helper('cms')->__('Store View'),
				'required' => true,
				'values' => Mage::getSingleton('adminhtml/system_store')->getStoreValuesForForm(false, true),
			));

			$renderer = $this->getLayout()->createBlock('adminhtml/store_switcher_form_renderer_fieldset_element');
			if ($renderer) {
				$field->setRenderer($renderer);
			}
		}
		else {
			if (($page = Mage::registry('brand_page')) !== null) {
				$page->setStoreId(Mage::app()->getStore()->getId());
			}
		}

		$fieldset->addField('is_enabled', 'select', array(
			'name' => 'is_enabled',
			'title' => $this->__('Enabled'),
			'label' => $this->__('Enabled'),
			'values' => Mage::getModel('adminhtml/system_config_source_yesno')->toOptionArray(),
		));
		
		$this->getForm()->setValues($this->_getFormData());

		return $this;
	}
}
