<?php  
/**
 * @category    Corra
 * @package     Corra_Brandpages 
 */
class Corra_Brandpages_Block_Adminhtml_Brandgrids_Grid extends Mage_Adminhtml_Block_Widget_Grid 
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('brandGrid');
        $this->setDefaultSort('page_id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(false);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('brandpages/brandpage')->getCollection();
        
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }
    
    /**
	 * Add the columns to the grid
	 *
	 */
	protected function _prepareColumns()
	{
		$this->addColumn('page_id', array(
			'header'	=> $this->__('ID'),
			'align'		=> 'right',
			'width'     => 1,
			'index'		=> 'page_id',
		));

		$this->addColumn('display_name', array(
			'header'	=> $this->__('Name'),
			'align'		=> 'left',
			'index'		=> 'display_name',
		));
		
		$this->addColumn('url_key', array(
			'header'	=> $this->__('URL Key'),
			'align'		=> 'left',
			'index'		=> 'url_key',
		));

		if (!Mage::app()->isSingleStoreMode()) {
			$this->addColumn('store_ids', array(
				'header'	  => $this->__('Store'),
				'align'		  => 'left',
				'index'		  => 'store_ids',
                'type'        => 'store',
                'store_all'   => true,
                'store_view'  => true,
                'sortable'    => false,
                'filter_condition_callback' => array($this, '_filterStoreCondition'),
				'options' 	  => $this->getStores(),
			));
		}

		$this->addColumn('is_enabled', array(
			'width'     => 1,
			'header'	=> $this->__('Status'),
			'index'		=> 'is_enabled',
			'type'		=> 'options',
			'options'	=> array(
				1 => $this->__('Enabled'),
				0 => $this->__('Disabled'),
			),
		));
	
		$this->addColumn('action', array(
			'header'  => $this->__('Action'),
			'type'    => 'action',
			'getter'  => 'getId',
			'actions' => array(array(
				'caption' => $this->__('Edit'),
				'url'     => array(
				'base'    => '*/brandpages/edit',
				),
				'field'   => 'id'
			)),
			'filter'   => false,
			'sortable' => false,
			'align'    => 'center',
		));

		return parent::_prepareColumns();
	}
}