<?php  
/**
 * @category    Corra
 * @package     Corra_Brandpages 
 */
class Corra_Brandpages_Block_Adminhtml_Brandgrids extends  Mage_Adminhtml_Block_Widget_Grid_Container 
{
	public function __construct()
  	{
    	$this->_controller = 'adminhtml_brandgrids';
		$this->_blockGroup = 'brandpages';
		$this->_headerText = Mage::helper('brandpages')->__('Collection Page Manager');
		parent::__construct();
		$this->_removeButton('add');
	}
}