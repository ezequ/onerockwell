<?php
/**
 * @category    Corra
 * @package     Corra_Brandpages
 */
class Corra_Brandpages_Block_Brandpage_View extends Mage_Core_Block_Template
{
	/**
	 * Adds the META information to the resulting page
	 */
	protected function _prepareLayout()
	{
		parent::_prepareLayout();
		return $this;
	}
    
    /**
     * Retrieves the current Brand model
     *
     * @return Corra_Brandpages_Model_Brandpage|null
     */
	public function getBrandPage()
	{
		if (!$this->hasBrandPage()) {
			if ($this->hasBrandPageId()) {
				$this->setBrandPage(Mage::getModel('brandpages/brandpage')->load($this->getSplashPageId()));
			}
			else {
				$this->setBrandPage(Mage::registry('brand_page'));
			}
                        
            return $this->getData('brand_page');
		} else {
			return Mage::registry('brand_page');
		}
	}
        
	public function hasBrandPage()
	{
		if(Mage::registry('brand_page'))
			return true;
		else
			return false;
	}

	/**
	 * Retrieves and renders the product list block
	 *
	 * @return string
	 */
	public function getProductListHtml()
	{
		return $this->getProductListBlock()->toHtml();
	}
	
	/**
	 * Retrieve the product list block
	 *
	 * @return Mage_Catalog_Block_Product_List
	 */
	public function getProductListBlock()
	{
		if ($block = $this->getChild('product_list')) {
			if (!$block->hasColumnCount()) {
				$block->setColumnCount(4);
			}

			return $block;
		}
		
		return false;
	}
	
	/**
	 * Retrieves the HTML for the CMS block
	 *
	 * @return string
	 */
	public function getCmsBlockHtml()
	{
		if (!$this->getData('cms_block_html')) {
			$html = $this->getLayout()->createBlock('cms/block')
				->setBlockId($this->getBrandPage()->getCmsBlock())->toHtml();

			$this->setData('cms_block_html', $html);
		}
		
		return $this->getData('cms_block_html');
	}
}
