<?php
class Corra_Brandpages_Adminhtml_BrandpagesController extends Mage_Adminhtml_Controller_Action
{
    public function indexAction()
    { 
       $this->loadLayout();
	   $this->_title($this->__("Brand Pages"));
	   $this->renderLayout();
    }
    
    public function newAction()
    { 
        return $this->_forward('edit');
    }
    
    public function editAction() 
	{
		$splash = $this->_initSplashPage();

		$this->loadLayout();
		$this->_setActiveMenu('attributeSplash');

		$this->_title('Corra');
		$this->_title('Attribute Brands');
		$this->_title($this->__('Brand Page'));

		if ($splash) {
				$this->_title($splash->getName());
		}

		$this->renderLayout();
    }
    
    protected function _initSplashPage() 
	{
		if (($page = Mage::registry('brand_page')) !== null) {
				return $page;
		}

		if ($id = $this->getRequest()->getParam('id')) {
			$page = Mage::getModel('brandpages/brandpage')->load($id);

			if ($page->getId()) {
					Mage::register('brand_page', $page);
					return $page;
			}
		}
		return false;
    }
    
    protected function _initValidAttributeSessionMessage() 
	{
		if (($page = $this->_initBrandPage()) !== false) {
			if ($page->getAttributeModel() && !$page->getAttributeModel()->getData('is_filterable')) {
					$page->getAttributeModel()->setIsFilterable(1)->save();
			}
		}
		return $this;
    }
    
    
    public function saveAction() 
	{
		if ($data = $this->getRequest()->getPost('brand')) {
			$page = Mage::getModel('brandpages/brandpage')
					->setData($data)
					->setId($this->getRequest()->getParam('id'));
			try {
				$this->_handleImageUpload($page, 'image');
				$this->_handleImageUpload($page, 'thumbnail');

				$page->save();
				$this->_getSession()->addSuccess($this->__('Brand page was saved'));
			}
			catch (Exception $e) {
				$this->_getSession()->addError($this->__($e->getMessage()));
			}

			if ($page->getId() && $this->getRequest()->getParam('back', false)) {
				return $this->_redirect('*/*/edit', array('id' => $page->getId()));
			}

			return $this->_redirect('*/brandpages');
		}

		$this->_getSession()->addError($this->__('There was no data to save.'));

		return $this->_redirect('*/brandpages');
    }
    
    protected function _initBrandPage()
	{
		if (($page = Mage::registry('brand_page')) !== null) {
			return $page;
		}

		if ($id = $this->getRequest()->getParam('id')) {
			$page = Mage::getModel('brandpages/brandpage')->load($id);

			if ($page->getId()) {
				Mage::register('brand_page', $page);
				return $page;
			}
		}

		return false;
    }
    
    protected function _handleImageUpload(Corra_Brandpages_Model_Brandpage $page, $field)
	{
		$data = $page->getData($field);

		if (isset($data['value'])) {
			$page->setData($field, $data['value']);
		}

		if (isset($data['delete']) && $data['delete'] == '1') {
			$page->setData($field, '');
		}

		if ($filename = Mage::helper('brandpages/image')->uploadImage($field)) {
			$page->setData($field, $filename);
		}
	}
        
        
    /**
	 * Delete a splash page
	 *
	 */
	public function deleteAction()
	{
		if ($pageId = $this->getRequest()->getParam('id')) {
			$brandPage = Mage::getModel('brandpages/brandpage')->load($pageId);
			
			if ($brandPage->getId()) {
				try {
					$brandPage->delete();
					$this->_getSession()->addSuccess($this->__('The Brand Page was deleted.'));
				}
				catch (Exception $e) {
					$this->_getSession()->addError($e->getMessage());
				}
			}
		}
		
		$this->_redirect('*/brandpages');
	}
}