<?php
/**
 * @category    Corra
 * @package     Corra_Brandpages
 */
class Corra_Brandpages_BrandpageController extends Mage_Core_Controller_Front_Action
{
	/**
	 * Display the brand page
	 *
	 * @return void
	 */
	public function viewAction()
	{
		if (($brandPage = $this->_initBrandPage()) === false) {
			return $this->_forward('noRoute');
		}
		
		// Register the brand layer model
		Mage::register('current_layer', Mage::getSingleton('brandpages/layer'));
        $this->loadLayout();
		if ($rootBlock = $this->getLayout()->getBlock('root')) {
			$rootBlock->addBodyClass('brand-page-' . $brandPage->getId());
			$rootBlock->addBodyClass('brand-page-' . $brandPage->getAttributeCode());
		}
					
		if ($headBlock = $this->getLayout()->getBlock('head')) {
			if ($title = $brandPage->getPageTitle()) {
				$headBlock->setTitle($title);
			}
			else {
				$this->_title($brandPage->getDisplayName());
			}

			if ($description = $brandPage->getMetaDescription()) {
				$headBlock->setDescription($description);
			}
			
			if ($keywords = $brandPage->getMetaKeywords()) {
				$headBlock->setKeywords($keywords);
			}
			
			$headBlock->addItem('link_rel', $brandPage->getUrl(), 'rel="canonical"');
		}
		
		if ($breadBlock = $this->getLayout()->getBlock('breadcrumbs')) {
			if (!$breadBlock->getSkipSplashPageHomeCrumb()) {
				$breadBlock->addCrumb('home', array('label' => $this->__('Home'), 'title' => $this->__('Home'), 'link' => Mage::getUrl()));
			}
			
                        $attrDetails = Mage::helper('brandpages')->getBrandAttrDetails();
                        $url = Mage::helper('brandpages')->getUrl('brands');
                        $breadBlock->addCrumb('brand_group', array('label' => $attrDetails['frontend_label'], 'title' => $attrDetails['frontend_label'], 'link' => $url));
				

			if (!$breadBlock->getSkipSplashPageCrumb()) {
				$breadBlock->addCrumb('brand_page', array('label' => $brandPage->getName(), 'title' => $brandPage->getName()));
			}
		}	

		$this->renderLayout();
	}
	
	
	/**
	 * Initialise the Brand Page model
	 *
	 * @return false|Corra_Brandpages_Model_Brandpage
	 */
	protected function _initBrandPage()
	{
		if (($page = Mage::registry('brand_page')) !== null) {                    
			return $page;
		}

		$brandPage = Mage::getModel('brandpages/brandpage')
			->setStoreId(Mage::app()->getStore()->getId())
			->load((int) $this->getRequest()->getParam('id', false));

		if (!$brandPage->getIsEnabled()) {
			return false;
		}
		return $brandPage;
	}
}
