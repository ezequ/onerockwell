(function ($) {
 $.fn.pushmenu = function (settings) {
 
 
  /* Cutomizable Options 
   ----------------------------------------------------------------------------*/
  var options = {
      button: "#pushmenu_trigger",
      wrapper:"body",
      target:".nav-container"
  };
   //Merge custom options
   $.extend(options, settings);
   
   $(document).delegate(options.button, "click", function(){
       
       
       if($(options.wrapper).hasClass("pm-open")){
          $(options.wrapper).addClass("pm-closing");           
          $(options.wrapper).removeClass("pm-open"); 
          setTimeout(function(){
              $(options.wrapper).removeClass("pm-closing");           
          }, 1000);

       }else{
        $(options.wrapper).addClass("pm-open").removeClass("pm-closing");           
       }
       return false;
   });
   var checkMenu = function(){
       if(CorraUI.isTablet()){
           $(options.target).removeClass("mega-menu").addClass("push-menu");
           $(options.target).accordion();
           $(options.wrapper).removeClass("pm-open pm-closing"); 
           $(options.target).removeClass("hmenu"); 

       }else{
           $(options.target).removeClass("push-menu").addClass("mega-menu");
           $(options.target).accordion().destroy();
           $(options.target).addClass("hmenu"); 
       }
   };
   checkMenu();
   
   $(window).resize(
           function(){
               if(CorraUI.viewportChanged()){
                checkMenu();
               }
           }
             
           );
 };

})(jQuery);

