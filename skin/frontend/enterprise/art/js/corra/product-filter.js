/**
 * @param {type} $
 * @returns {undefined}
 * 
 */


(function ($) {
    $.fn.productFilter = function (settings) {
        var defaults = {
            filterLink: ".jq-ajax-filter",
            contentGrid: ".jq-product-grid",
            sortFilter: ".jq-sort",
            appliedFilter: ".jq-ajax-filter-button",
            //resetFilter: ".jq-reset-filter",
            toolbar: ".jq-ajax-toolbar",
            pager: ".pager",
            loader: "#wait",
            focusEnabled: false,
            focusTop: '.jq-content-top',
            loadMore:'.jqLoad'
        };
        var options = $.extend(defaults, settings);

        var init = function () {
            var currentURL = document.URL;
            applyFilter(currentURL, 'init');
            var filterLink = options.filterLink + ' a';
            var appliedFilterLink = options.appliedFilter + ' a';
            var toolbarLink = options.toolbar + ' a';
            var sortFilter = options.sortFilter;
            $('body').undelegate(filterLink, "click", applyFilter).delegate(filterLink, 'click', applyFilter);
            $('body').undelegate(appliedFilterLink, "click", applyFilter).delegate(appliedFilterLink, 'click', applyFilter);
            $('body').undelegate(toolbarLink, "click", applyFilter).delegate(toolbarLink, 'click', applyFilter);
            $('body').undelegate(sortFilter, "change", applySort).delegate(sortFilter, 'change', applySort);
        };

        var applySort = function () {
            var url = $(this).val();
            applyFilter(url);
        };

        var applyFilter = function (link, state) {

            //show loader
            var loader = $(options.loader);
            if (state != 'init') {
                loader.fadeIn();
            } else {
                if (typeof (location.search) != 'undefined' && location.search == '') {
                    return false;
                }
            }

            if (typeof link != 'string') {
                var link = $(this).attr('href');
                $(this).toggleClass('active')
            }

            //For IE < 10
            /*if (typeof (history.pushState) == 'undefined') {
                link = removeAjaxLink(link);
                location.replace(link);
            }*/

            if (link.indexOf('ajax=true') < 0) {
                if (link.indexOf('?') > 0) {
                    link = link + "&ajax=true";
                } else {
                    link = link + "?ajax=true";
                }
            }

            $.ajax({
                url: link,
                context: document.body
            }).done(function (data) {

                $data = $($.parseHTML(data, false));
                //apply filter and update contents
                updateContents($data);
                
                updateAddressBar(link);
                
                handleLoadMoreBtn();

                //hide loader
                loader.fadeOut();

            });
            return false;
        };
        
        var handleLoadMoreBtn = function(){
            var loadMore = $(options.loadMore).closest('.jqload-block');
            if($('.pager .pages ol li.current').next('li').length > 0){
                loadMore.show();
            } else {
                loadMore.hide();
            }
        };

        var updateContents = function ($data) {

            var sortFilterFirst = options.sortFilter + ':first';
            var toolbarFirst = options.toolbar + ':first';
            //product grid repace
            $('body').find(options.contentGrid).replaceWith($data.find(options.contentGrid));
            
            //sort block replace
            $('body').find(options.sortFilter).replaceWith($data.find(sortFilterFirst));
            
            //applied filter list block replace 
            $('body').find(options.appliedFilter).replaceWith($data.find(options.appliedFilter));
            
            //toolbar block replace
            $('body').find(options.toolbar).replaceWith($data.find(toolbarFirst));
            
            //filter link block rebuild
            renderNavigation($data);
            
            //pager next link update
            var nextLink = jQuery(options.pager).find('li.current').next('li').find('a').attr('href');
            $(options.loadMore).attr('href', nextLink);
            
            //Content focus
            if (options.focusEnabled && $(options.focusTop).length > 0) {
                $('html,body').scrollTop($(options.focusTop).offset().top);
            }

        };

        var renderNavigation = function ($data) {
            var ajaxResponse = $data.find(options.filterLink);
            var existing = $(options.filterLink);
            existing.find('a').parent().addClass('disabled');
            ajaxResponse.find('a').each(function () {
                var item = jQuery(this).parent();
                existing.find('#' + item.attr('id')).replaceWith(item);
            });
            existing.fadeIn();
        };

        var updateAddressBar = function (url) {
           url = removeAjaxLink(url);
            if (typeof (history.pushState) == 'function') {
                history.pushState(null, null, url);
                //history.replaceState(null, null, url);
            } else {
                location.replace(url);
            }
        };
        
        var removeAjaxLink = function(url){
             return url.replace("ajax=true", "").replace("?&", "?").replace(/&\s*$/, "");;
        };
        init();
        return this;
    }
    ;
})(jQuery);



    