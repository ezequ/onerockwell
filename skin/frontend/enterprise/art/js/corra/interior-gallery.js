/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
(function ($) {

    $(document).ready(function () {

        $('#gallery_images').owlCarousel({
            nav: true,
            dots: false,
            items: 1,
            loop: true,
            margin: 10,
            responsive: {
                0: {
                },
                600: {
                    stagePadding: 100
                },
                900: {
                    stagePadding: 150
                },
                1000: {
                    stagePadding: 200
                },
                1200: {
                    stagePadding: 250
                },
                1400: {
                    stagePadding: 300
                },
                1600: {
                    stagePadding: 350
                },
                1800: {
                    stagePadding: 400
                }
            }
        });

        initResize();
        $(window).resize(initResize);

    });

    var initResize = function () {
        $('#gallery_images').trigger('refresh.owl.carousel');

        if (CorraUI.isMobile()) {
            $('.gallery-items').owlCarousel({
                nav: false,
                dots: true,
                items: 2,
                loop: false
            });
        } else {
            if (typeof $('.gallery-items').data('owlCarousel') != 'undefined') {
                $('.gallery-items').owlCarousel({
                    responsive: false
                });
                $('.gallery-items').data('owlCarousel').destroy();
                $('.gallery-items').removeClass("owl-carousel owl-theme owl-loaded");
                $('.gallery-items .item').unwrap();
                $('.gallery-items').removeData();
            }
        }
    };

})(jQuery);