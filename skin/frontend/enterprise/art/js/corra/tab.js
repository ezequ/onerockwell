(function ($) {

    $.fn.tab = function (settings) {

        //Cutomizable Options 
        var options = $.extend({
            "head": '.jq-tab-head',
            "body": '.jq-tab-body',
            "group": '.jq-tab-group',
            "openedClass": 'open',
            "multiple": false
        }, settings);

        $(this.selector).each(function () {
            $(this).find(options.head).off('.tab')
                    .on('click.tab', toggleTab);
            $(this).find(options.body).hide();
            $(this).find(options.head + ":first").addClass(options.openedClass);
            $(this).find(options.body + ":first").show();
        });
        this.destroy = function(){
            
            $(this).find(options.head).off("click");
            $(this).find("."+options.openedClass).removeClass(options.openedClass);
            $(this).find(options.body).css({"display":""});
        };
        function toggleTab() {
            
            var _this = $(this);
            _this.siblings(options.head).removeClass(options.openedClass);
            _this.addClass(options.openedClass);
            
            
            var myParent = _this.closest(options.group);
            var myIndex = myParent.find(options.head).index(_this);
            
           /* _this.siblings(options.body).hide();
            _this.siblings(options.body).eq(myIndex).show();*/
            
            
           var myBody = myParent.find(options.body).eq(myIndex);
           myBody.siblings(options.body).hide();
          // myParent.find(options.head).removeClass(options.openedClass);
           
          // _this.addClass(options.openedClass);
           myBody.show();
            
                
            return false;
        }
        return this;
    };
})(jQuery);

