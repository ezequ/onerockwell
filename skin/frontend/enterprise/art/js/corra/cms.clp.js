(function ($) {

    $(document).ready(function () {

        /* Carousel for Product help section
         ----------------------------------------------------------------------------*/

        var itemsLength = $("#jq-project-help-section").find(".item").length;

        $('#jq-project-help-section').owlCarousel({
            nav: false,
            dots: true,
            items: 1,
            loop: true,
            margin: 55,
            responsive: {
                0: {
                    stagePadding: 65,
                    margin: 33
                },
                480: {
                    stagePadding: 120,
                    margin: 20
                },
                500: {
                    stagePadding: 150,
                    margin: 20
                },
                600: {
                    stagePadding: 166,
                    margin: 39
                },
                767: {
                    items: 3,
                    loop: false,
                    dots: itemsLength > 3 ? true : false,
                    margin: 20
                },
                1160: {
                    items: 3,
                    loop: false,
                    dots: itemsLength > 3 ? true : false,
                }
            }
        });
       
        addProductGridClasses();

    });

    /* ADD CLASSES TO PRODUCT GRID
     ----------------------------------------------------------------------------*/

    function addProductGridClasses() {
        $('.product-grid li.item:nth-of-type(2n+2)').addClass('mRow');
        $('.product-grid li.item:nth-of-type(3n+3)').addClass('tRow');
        $('.product-grid li.item:nth-of-type(4n+4)').addClass('dRow');
        $('.product-grid li.item:nth-of-type(4n+5)').addClass('nRow');
    }

})(jQuery);