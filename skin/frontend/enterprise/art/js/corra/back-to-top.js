(function ($) {
    $(document).ready(function () {
        $(window).scroll(function () {
            adjustTopButton();
        });

        $(window).resize(function () {
            adjustTopButton();
        });
        
        $(document).delegate('.jqTop', 'click', function () {
            jQuery('html, body').animate({
                scrollTop: 0
            }, 500);
        });

        createTopButton();

    });

    function createTopButton() {
        var div = $("<div>", {"class": "back-to-top"}).hide();
        var toTopButton = $("<a>", {"href": "javascript:;", "class": "jqTop"}).html("BACK TO TOP");
        toTopButton.appendTo(div);
        div.appendTo(".col-main");
        adjustTopButton();
    }

    function adjustTopButton() {
        var scrollTop = $(window).scrollTop();
        var jqTop = jQuery('.back-to-top');
        if (scrollTop > 100) {
            jqTop.fadeIn();
        } else {
            jqTop.fadeOut();
        }

        /*
         * Back to top button not moving to footer sectoin
         */
        var colMain = $('.col-main');
        var footerTop = $('.footer-container').offset().top;
        var docHeight = $(document).height();
        var docWidth = $(document).width();
        var topBtnBottm = docHeight - footerTop;
        
        var winHeight = window.innerHeight;
        var fixedPosBottom = 130;

        if ((winHeight + scrollTop - fixedPosBottom - jqTop.height()) > footerTop) {
            jqTop.css({
                position: 'absolute',
                bottom: topBtnBottm,
            });
        } else {
            jqTop.css({
                position: 'fixed',
                bottom: fixedPosBottom,
            });
        }
        if (!CorraUI.isMobile()) {
            var topBtnRight = docWidth - (colMain.offset().left + colMain.width());
            jqTop.css({
                right: topBtnRight
            });
        } else {
            jqTop.css({
                right: ''
            });
        }
    }
})(jQuery);
