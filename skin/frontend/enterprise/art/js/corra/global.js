var Global = (function ($) {
    var ready = function () {
        CorraUI.init({
            desktop: 1160,
            tablet: 989,
            mobile: 767
        });

        pushMenu();
        moveElements();
        createAccordion();
        initializeUniformJs();

        $(window).resize(initResize).scroll(initScroll);
        $("#top-search-icon").click(showSearchBox);
       // Ajax.Responders.register({onComplete: initializeUniformJs});

        $('.top-cart')
                .mouseenter(function () {
                    !CorraUI.isTouch() && $('#topCartContent').show();
                })
                .mouseleave(function () {
                    !CorraUI.isTouch() && $('#topCartContent').hide();
                });

        //Slim scroll added to minicart
        $("#mini-cart li").length > 2 && $("#mini-cart").slimScroll({
            height: '260px',
            railVisible: true,
            alwaysVisible: true,
            railColor: '#f3f4f4',
            color: '#e3e4e5',
            size: '8px',
            disableFadeOut: false,
            borderRadius: 0,
            railBorderRadius: 0
        });
        
        function build_url(accordionSelector,url){
           var _hyphen = "";
           $(accordionSelector).each(function(){
               if($(this).data('value')){
                url += _hyphen + $(this).data('value');
                _hyphen = "-";
               }
           });
           return url;
        }
        
        $("#nav .mega-menu-wrapper > ul > li.tile-finder .jq-tile-finder > li").click(function(){
             $(this).parent().prev().text($(this).text()).click();
           $(this).parent().data('value',$(this).attr('data-value'))
           var finderUrl = $("#jq-finder-link").data("href"); 
           
           finderUrl = build_url(".jq-tile-finder",finderUrl)
          
           $("#jq-finder-link").attr("href", finderUrl);
          
           //if($(this).val() != ''){
               $('#jq-material-menu-error').hide();
           //}
        });
        
        $("#jq-finder-link").click(function(){
            var materialMenuError = $('#jq-material-menu-error');
            var materialSelect = $('.jq-tile-finder#material')
            var productTypeSelect = $('.jq-tile-finder#product_type')
            var colorFamilySelect =  $('.jq-tile-finder#color_family')
            if(typeof materialSelect.data('value') == 'undefined' && typeof productTypeSelect.data('value') == 'undefined' && typeof colorFamilySelect.data('value') == 'undefined'){
                materialMenuError.fadeIn();
                return false;
            } else {
                materialMenuError.hide();
            }
        });
        
        $('#nav .mega-menu-wrapper > ul > li.collections .jq-collection-finder > li').click(function(){
           $(this).parent().prev().text($(this).text()).click();
            
           $(this).parent().data('value',$(this).attr('data-value'))
           var finderUrl = $('#jq-collection-finder-link').data("href"); 
           finderUrl = build_url(".jq-collection-finder",finderUrl)
       
           $("#jq-collection-finder-link").attr("href", finderUrl);
          // if($(this).val() != ''){
               $('#jq-collection-menu-error').hide();
          // }
        });
        
         $("#jq-collection-finder-link").click(function(){
            var collectionMenuError = $('#jq-collection-menu-error');
            var collectionSelect = $('#web_collection_name');
            if(typeof collectionSelect.data('value') == 'undefined'){
                collectionMenuError.fadeIn();
                return false;
            } else {
                collectionMenuError.hide();
            }
         });
         
         $('#nav .jq-accordian-head').on('click',function(){
             $('#nav .jq-accordian-head.open').not($(this)).click()
         });
         $('#nav .jq-accordian-group').accordion({head:'.jq-accordian-head',body:'.jq-accordian-body'})
         
         //Close all open accordions, when mega menu closes.
         $('#nav > li').mouseleave(function(){
              $('#nav .jq-accordian-head.open').click();
         });
         
        /* Click event handler for Right BreadCrumbs Section 
         ----------------------------------------------------------------------------*/
        $('.right a').on("click", function () {
            var id = "#" + $(this).data('id');
            var headerHeight = $(".header-container").height();
            var top = $(id).offset().top;
            var scrollDiff = top - headerHeight;
            $('html,body').animate({scrollTop: scrollDiff - 50}, 1000);
        });
         
    }//Doc ready ends here


    var initScroll = function () {
        stickyHeader();
        
    };

    var initResize = function () {
        if (CorraUI.viewportChanged()) {
            stickyHeader();
            moveElements();
            createAccordion();
            initializeUniformJs();
        }

    };

    var moveElements = function () {
      var searchForm = $('#search_mini_form');
        if (CorraUI.isMobile() || CorraUI.isTablet()) {
          searchForm.addClass('show-search-box');
          $('#nav').before(searchForm).after($('.top-myaccount')).after($('.loc-phone-wrapper'));
            /* DO NOT REMOVE THESE COMMENTED SECTIONS */
            //$(".branding").append($(".top-cart"));
            //$(".branding").append($(".store-location"));
            //$("#pushmenu_trigger").after($("#top-search-icon"));
            //nav-container

            $()

        } else {
            /* DO NOT REMOVE THESE COMMENTED SECTIONS */
            //$(".header").prepend($(".store-location"));
            //$(".quick-access").append($(".top-cart"));
            //$(".quick-access").append($("#top-search-icon"));
            searchForm.removeClass('show-search-box');
            $('.header-container').after(searchForm);
            $('#top-search-icon').before($('.loc-phone-wrapper'));
           $('.cart-myaccount-wrapper').prepend($('.top-myaccount'));
        }

    };

    var pushMenu = function () {
        $('#nav').pushmenu({target: '.nav-container', button: "#pushmenu_trigger"});
    };

    var createAccordion = function () {
        var accordionSelectors = ".footer";

        if (CorraUI.isMobile() || CorraUI.isTablet()) {
            $(accordionSelectors).accordion();
            $(".footer-center").after($(".social-icon"));

             $('#nav').accordion();
        } else {
            $(accordionSelectors).accordion().destroy();
            $(".footer-top .newsltr").after($(".social-icon"));

             $('#nav').accordion().destroy();
        }
    };

    var stickyHeader = function () {

        if (CorraUI.isDesktop()) {
            if ($(document).scrollTop() > 0) {
                $('body').addClass('has-sticky-header');
            } else {
                $('body').removeClass('has-sticky-header');
            }
        } else {
            $('body').removeClass('has-sticky-header');
        }

    };

    var showSearchBox = function () {
        $("#search_mini_form").toggleClass('show-search-box');
        return false;
    };
    
    var initializeUniformJs = function () {
        var uniformSelector = $("select, input:checkbox, input:radio").not('.no-uniform');
        uniformSelector.uniform({selectAutoWidth: false});
        uniformSelector.change(function () {
            $.uniform.update();
        });
    };


    return {
        ready: ready
    };
})(jQuery)
jQuery(Global.ready);

