var PLP = (function ($) {
    var loadedProductUrl = '';
    var ready = function () {
        $(window).scroll(initScroll);
        $(window).resize(initResize);
        $(document).click(domClick);

        addGridItemClass();
        createFilterAccordion();
    };
    var initScroll = function () {
        lazyLoadProducts();
    };
    var initResize = function () {
        if (CorraUI.viewportChanged()) {
            
            createFilterAccordion();
        }

    };
    //DOM click to close open drop down
    var domClick = function(e){
        var sortAccordion = $('#jq-sort-head.jq-accordion-head.open');
        if ($(e.target).closest('.sorter').length == 0) {
            sortAccordion.removeClass('open').next('.jq-accordion-body').slideUp();
        }
        
        var filterAccordion = $('#jq-block-layered-nav .jq-accordion-head.open');
        if ($(e.target).closest('#jq-block-layered-nav').length == 0) {
            filterAccordion.removeClass('open').next('.jq-accordion-body').slideUp();
        }
        
    };

    //check and trigger loading of next set of products
    var lazyLoadProducts = function () {
        var docViewTop = $(window).scrollTop();
        var docViewBottom = docViewTop + $(window).height();
        var contentGrid = $('ul.products-grid');
        var contentGridBottom = contentGrid.offset().top + contentGrid.height();
        if (docViewBottom > contentGridBottom) {
            if (lazyloadTimer) {
                clearTimeout(lazyloadTimer);
            }
            var lazyloadTimer = setTimeout(loadNext, 100)
        }
    };

    //load next set of images.
    var loadNext = function () {
        var currentPager = $('.pages ol li.current:last');
        var nextPager = currentPager.next('li').find('a');
        if (nextPager.length == 0) {
            return false;
        }
        if (typeof PLP != '' && loadedProductUrl == nextPager.attr('href')) {
            return  false;
        }
        loadedProductUrl = nextPager.attr('href');

        var loadMoreLoader = $('#load-more-loader');

        loadMoreLoader.show();
        $.ajax({
            url: nextPager.attr('href'),
            dataType: 'html',
            type: 'get',
            success: function (data) {
                currentPager.removeClass('current');
                nextPager.addClass('current');
                var response = $('<div/>').html(data);
                var productsList = response.find('.products-grid').html();
                var pagerAmt = response.find('.pager .amount').html();
                var pages = response.find('.pages:first').html();
                $('.products-grid').append(productsList);
                $('.pager .amount').html(pagerAmt);
                $('.pages').html(pages);

                addGridItemClass();
                loadMoreLoader.hide();
            }
        });
        return false;
    };

    //add grid class to item 
    var addGridItemClass = function () {
        //$('.products-grid li:nth-of-type(n+2)').addClass('mRow');
        $('.products-grid li:nth-of-type(2n+3)').addClass('tRow');
        $('.products-grid li:nth-of-type(4n+5)').addClass('dRow');
    };

    //rearrange navigation html 
    var rearrangeNavigation = function () {
        var layeredNavBlock = $('#jq-block-layered-nav');
        if (CorraUI.isTablet() || CorraUI.isMobile()) {
            $('.category-view .toolbar').prepend(layeredNavBlock);
        } else {
            $('.col-left.sidebar .mb-mana-catalog-leftnav').append(layeredNavBlock);
        }
    }

    var createFilterAccordion = function () {
        rearrangeNavigation();
        
        var sortTitle = $('#jq-sort-head + .sort-by li.disabled a').html();
        if (!sortTitle) {
            sortTitle = 'SORT BY:';
        }
        $('#jq-sort-head').html(sortTitle);
        
        //Sort accordion
        $('.sorter').accordion();
        
        //Filter accordion
        if (CorraUI.isTablet() || CorraUI.isMobile()) {
            $(".narrow-by-list dt").addClass("jq-accordion-head");
            $(".narrow-by-list dd").addClass("jq-accordion-body");
            $('#jq-block-layered-nav').accordion();
        } else {
            $(".narrow-by-list dt").removeClass("jq-accordion-head");
            $(".narrow-by-list dd").removeClass("jq-accordion-body");
            $(".narrow-by-list dd").show();
            $('#jq-block-layered-nav').accordion().destroy();
        }
    }

    return {ready: ready, createFilterAccordion: createFilterAccordion};
})(jQuery);
jQuery(PLP.ready);
