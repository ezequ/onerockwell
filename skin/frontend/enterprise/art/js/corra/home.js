var Home = (function ($) {
    var current = 2;

    var ready = function () {
        $(window).resize(initResize).scroll(initScroll);

        $('.owl-lazy,.slider-img').corraResponsiveImage({
            mob: "mob",
            tab: "tab",
            desk: "desk"
        });

        $('#main_slider').owlCarousel({
            items: 1,
            loop: true,
            dots: false,
            lazyLoad: false,
            autoheight: true,
            responsiveRefreshRate: '200ms',
            transitionStyle: "fade",
            responsive: {
                0: {},
                600: {},
                1160: {
                    autoplay: true,                    
                    autoplayTimeout: 7000,
                    autoplayHoverPause: true,
                }
            }

        });

        $(".image-title").find("a:first-child").addClass("active");
        $(".vert-slider-list").find("li:first-child").addClass("active");

// Main slider change event
        $('#main_slider').on('changed.owl.carousel', function (property) {
            closeHSContent();
            current = property.item.index;
            $('.image-title .owl-tag').removeClass('active');
            $('.image-title .owl-tag').eq(current - 2).addClass('active');
            var selectedText = $('.image-title .owl-tag').eq(current - 2).text();
            if (current === 6) {
                $('.image-title .owl-tag').eq(0).addClass('active');
                selectedText = $('.image-title .owl-tag').eq(0).text();
            }
            $("#slider-head").text(selectedText);
            textRearrange();
        });

// Image slider control click events
        $(".image-title a").on("click", function () {
            var selectedId = $(this).data("id");
            if (selectedId === 0) {
                $("#main_slider").trigger("to.owl.carousel", [0, 500, true]);
            } else if (selectedId === 1) {
                $("#main_slider").trigger("to.owl.carousel", [1, 500, true]);
            } else if (selectedId === 2) {
                $("#main_slider").trigger("to.owl.carousel", [2, 500, true]);
            } else if (selectedId === 3) {
                $("#main_slider").trigger("to.owl.carousel", [3, 500, true]);
            }
        });

// Vertical slider tool tip click events
        $('.vert-slider-list li').on("click", function () {
            var id = "#" + $(this).find("a").data('id');
            var headerHeight = $(".header-container").height();
            var top = $(id).offset().top;
            var scrollDiff = top - headerHeight;
            if (CorraUI.isMobile() || CorraUI.isTablet()) {
                $('html,body').animate({scrollTop: scrollDiff}, 1000);
            } else if (CorraUI.isDesktop() && id === "#slider_0") {
                $('html,body').animate({scrollTop: 0}, 1000);
            } else if (CorraUI.isDesktop() && id !== "#slider_0" && headerHeight === 80) {
                $('html,body').animate({scrollTop: scrollDiff}, 1000);
            } else {
                $('html,body').animate({scrollTop: top - 133}, 1000);
            }
        });

        sliderAccordion();
        textRearrange();

// Hot spot click event
        $(".hotspot").on("click", function () {
            
            var elmMainSlider = jQuery('#main_slider');
            var elmSliderOneWrap = jQuery('#sliderone-wrapper');
            //stop auto slide when hs verlay is active
            elmMainSlider.data('owl-carousel').settings.autoplay = false;

            //add hs active class
            elmSliderOneWrap.addClass('hs-loaded');

            skuValue = $(this).data("product-sku");
            data = HotSpotProducts[skuValue];

            $('<img/>').attr('src', data.imageUrl).load(function() {
                elmSliderOneWrap.removeClass('hs-loaded');
                $(this).remove(); // prevent memory leaks 
                $('body').css('background-image', 'url(http://picture.de/image.png)');


                
                
                //$(".fp-image-large").attr("src",data.imageUrl);
                $('.fp-wrapper').css({'background-image': 'url(' + data.imageUrl + ')'})
                $(".fp-info h3").text(data.name);
                $(".fp-info h4").text(data.subtitle);
                $(".fp-pdp-link").attr("href", data.productUrl);
                if (!CorraUI.isMobile()) {
                    $(".fp-image-thumb").attr("src", data.thumbImageUrl);
                    $(".fp-desc").text(data.description);
                } else {
                    $(".mobile-close").show();
                }
                $(".fp-wrapper").fadeIn(600);
            });
        });

//Closing Hot Spot Pop-up
        $(".fp-close, .mobile-close").on("click", function () {
            var elmMainSlider = jQuery('#main_slider');
            closeHSContent();
            if (!(CorraUI.isMobile() || CorraUI.isTablet())) {
                elmMainSlider.data('owl-carousel').settings.autoplay = true;
            }
            jQuery('#sliderone-wrapper').removeClass('hs-loaded');
        });

    }


    var initScroll = function () {
        //stickyHeader();
        
        // selecting vertical slider dots on scroll

        var scrollPos = $(document).scrollTop();
        $('.vert-slider-list li').each(function () {
            var id = "#" + $(this).find("a").data('id');
            var headerHeight = $(".header-container").height();
            var top = $(id).offset().top;
            var scrollDiff = top - headerHeight;
            if (scrollPos === 0){
                $('.vert-slider-list li').removeClass("active");
                $(".vert-slider-list").find("li:first-child").addClass("active");
            } else if(scrollDiff <= scrollPos && scrollDiff + headerHeight > scrollPos) {
                $('.vert-slider-list li').removeClass("active");
                $(this).addClass("active");
            }
            
        });
    };


    var initResize = function () {
         if (typeof resizeDelay != 'undefined') {
            clearTimeout(resizeDelay);
        }
        var resizeDelay = setTimeout(function () {
            sliderAccordion();
            textRearrange();
        }, 50);
        /*if (CorraUI.viewportChanged()) {
            //moveElements();
            sliderAccordion();
            textRearrange();
        }*/
    };

// Function to arrange image slider text
    var sliderAccordion = function () {
        var accordionSelectors = ".sliderone-container";
        if (CorraUI.isMobile()) {
            $(".image-title").addClass("jq-accordion-body body-slider");
            $(accordionSelectors).accordion();
            $(".image-title a").on("click", function () {
                var selectedText = $(this).text();
                $("#slider-head").text(selectedText);
                $(".image-title.jq-accordion-body").hide();
                $(".jq-accordion-head.head-slider").removeClass("open");
            });
        } else {
            $(accordionSelectors).accordion().destroy();
            $(".image-title").removeClass("jq-accordion-body.body-slider");
        }
    };

// Image text (for Block 1 and Block 2) positions changed in desktop and mobile view.
    var textRearrange = function () {
        if (CorraUI.isMobile()) {
            $("div").next(".navButton").remove();
            $(".owl-item").eq(current).find(".navButton").clone().insertAfter($('.sliderone-container'));
            $(".art-diff").after($(".art-text"));
        } else {
            $("div .sliderone-container").next(".navButton").remove();
            $("#main_slider .owl-item.active a.main-slider-image").after($('.sliderone-container .jq-accordion-group .navButton'));
            $(".art-diff h4").after($(".art-text"));
        }
    };

//Function to close Hot spot Pop-up
    var closeHSContent = function () {
        $(".fp-image-large").attr("src", "");
        $(".fp-image-thumb").attr("src", "");
        $(".fp-info h3").text("");
        $(".fp-info h4").text("");
        $(".fp-desc").text("");
        $(".fp-pdp-link").attr("href", "");
        $(".fp-wrapper").hide();
        $(".mobile-close").hide();
    };

    return {
        ready: ready
    };
})(jQuery);
jQuery(Home.ready);
        