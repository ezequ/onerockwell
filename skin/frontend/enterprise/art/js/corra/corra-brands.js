// JavaScript Document
jQuery(function () {   
   jQuery('input#brandsearch').quicksearch('.alphabet-brand-list div ul li a.brandlink');
   jQuery("li[class^='brand level-more-']").hide();
   
   jQuery(".brand-holder").click(function(){
       jQuery(".selector-options").slideToggle();
       jQuery(".selector-toggle").toggleClass('up','down');
   });
   
   jQuery(".selector-options .alphabet-set").click(function(){
      var rel = jQuery(this).attr("rel"); 
      var label = jQuery(this).html();
      jQuery(".selector-index").html(label);
      jQuery("#brand-key-list").hide();
      jQuery("#brand-full-list").show();
      jQuery(".brand-full-list .lists").hide();
      jQuery(".list"+rel).show();
      
   });
   
   //jQuery('.brandlink').hover(function(){
   jQuery(document).delegate('.brandlink', 'hover', function(){
       jQuery(this).nextAll('.brands-popup:first').show();
   });
   
   //jQuery('.close-div').click(function(){
   jQuery(document).delegate('.close-div', 'click', function(){
       jQuery(this).closest('.brands-popup').hide();
   });
   
   jQuery(document).ready(function(){
       var listSet = window.location.hash.substr(1);
       if(listSet >= 1 && listSet <= 6){
           showBlock(listSet);
       } else {
        if(jQuery(".level-"+listSet).length>0){
            viewAll(listSet);
            jQuery("#megamenu-desktop").hide();
            jQuery("#megamenu-desktop").removeClass("show");
        }
       }
   });
   
   jQuery(".second-level-menu .list-set .view-all a").click(function(){
       jQuery("#trigger-menu-large").click();
   });
   
   jQuery(window).on('hashchange',function(){ 
       window.location.reload();
       return;
   });
});

function showBlock(id){
    if(id == "all"){
        jQuery(".alphabet-set").removeClass("active");
        jQuery("#brand-full-list .lists").show();
        jQuery("#brand-full-list").show();
        jQuery("#brand-key-list").hide();
    } else {
        jQuery(".alphabet-set").removeClass("active");
        jQuery(".set"+id).addClass("active");
        jQuery("#brand-key-list").hide();
        jQuery("#brand-full-list").show();
        jQuery("#brand-full-list .lists").hide();
        jQuery("#brand-full-list .list"+id).show();
    }
}

function showlist(id){
    jQuery(".brandlist .lists").hide();
    jQuery("#list-"+id).show();
    jQuery(".alphabet-menu-set").removeClass("active");
    jQuery("#"+id).addClass("active");
    if(id== "set5"){
        jQuery("#"+id).parent().parent().parent().addClass("last-group");
    } else {
        jQuery(".children-wrapper.brandlist").removeClass("last-group");
    }
}

function viewAll(id){
    jQuery("#brand-full-list").hide();
    jQuery(".alphabet-set").removeClass("active");
    var i =0;
    if(id == 'num')
        var key = "#";
    else
        key = id.toUpperCase();
    var html ='<div class="key-title">'+key+'</div>';
    jQuery(".level-"+id+" ul li").each(function(){
       if(i==0)
           html += "<ul>";
       if(jQuery(this).find(".brands-popup").length > 0){
           html += "<li class='flyout brand'>"+jQuery(this).html()+"</li>";
       } else {
           html += "<li class='brand'>"+jQuery(this).html()+"</li>";
       }
       i++;
       
       if(i==10) {
           html += "</ul>";
           i=0;
       }
    });
    jQuery("#brand-key-list").html(html);
    jQuery("#brand-key-list").show();
    jQuery('html, body').stop().animate({
	        'scrollTop': jQuery("#brand-key-list").offset().top
	    }, 900, 'swing');
//    if(type=="show"){
//        jQuery(".level-more-"+id).show();
//        jQuery("#view-all-"+id).hide();
//        jQuery("#show-less-"+id).show();
//    } else {
//        jQuery(".level-more-"+id).hide();
//        jQuery("#view-all-"+id).show();
//        jQuery("#show-less-"+id).hide();
//    }
}

function readMore(id){
    if(id==0){
        jQuery(".splash-description").hide();
        jQuery(".splash-description-full").show();
    } else {
        jQuery(".splash-description").show();
        jQuery(".splash-description-full").hide();
    }
    
}


