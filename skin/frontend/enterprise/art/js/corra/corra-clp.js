function adjustTopButton(){
    var jqTop = jQuery('.jqTop');
    if(document.body.clientWidth >= 960 && document.body.clientWidth <= 1200) {
        jqTop.css('right','15px');
    } else {
        jqTop.css('right','');
    }
    var docViewTop = jQuery(window).scrollTop();
        if (docViewTop > 100) {
            var docViewBottom = docViewTop + jQuery(window).height();
            if(jQuery(".col1-layout").length > 0)
                var elemBottom = jQuery(".col1-layout").offset().top + jQuery(".col1-layout").height();
            else
                var elemBottom = jQuery(".col2-left-layout").offset().top + jQuery(".col2-left-layout").height();
            jqTop.fadeIn();
            if ((elemBottom < docViewBottom))
            {
                if(jQuery(".col1-layout").length > 0)
                    jqTop.css({'position': 'absolute', 'top': jQuery(".footer-container").offset().top - jqTop.height() - 50});
                else
                    jqTop.css({'position': 'absolute', 'top': jQuery(".footer-container").offset().top - jqTop.height() - 50});
            } else {
                jqTop.css({'position': 'fixed', 'top': ''});
            }

        } else {
            jqTop.fadeOut();
        }
}

function moveToTop() {
    if(document.body.clientWidth > 997){
         jQuery('html, body').animate({
            scrollTop: 0
         }, 500);
    }
}

(function($) {
    $(document).ready(function() {
		//updateCopmareIcons();
		          jQuery('.catblocks li:nth-child(4n+5)').addClass('dRow');
		          //jQuery('.catblocks li:nth-child(4n+5)').addClass('tRow');
                          jQuery('.catblocks li:nth-child(2n+3)').addClass('mRow');
					
        $(document).delegate('.jqTop', 'click', function() {
            $('html, body').animate({
                    scrollTop: 0
                }, 500);
        });
        
        if($("#new-products-home").find('li').length > 0) {
                var owl = $("#new-products-home");
                owl.owlCarousel();
                $("#new-products-home").data('owlCarousel').destroy();
                   owl.owlCarousel({
                       
                       
                   items : 5, //5 items above 1000px browser width
                   itemsDesktop : [960,5], //5 items between 1000px and 960px
                   itemsDesktopSmall : [959,3], // betweem 959px and 768px
                   itemsTablet: [767,2], //2 items between 480 and 0
                   itemsMobile: [479,1], //2 items between 600 and 0    
                   navigation : true,
                   pagination : false,
                   rewindNav : false
                   });
           }
    });
    
    $(window).scroll(function () {
        adjustTopButton();
    });
})(jQuery);


