/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
(function ($) {

    var getUrl, getId, wishlistMessage = "";

    $(document).ready(function () {

        /** Click event - Sign up **/
        $(document).delegate('#sign-up', 'click', function () {
            $("#login-register-forms").show();
            $('#jq-login-form').hide();
            $('#jq-register-form').show();
        });
        
        /** Click event - Sign In **/
        $(document).delegate('#member-form', 'click', function () {
            $("#login-register-forms").show();
            $('#jq-login-form').show();
            $('#jq-register-form').hide();
        });

        /** Click event - Forgot Password **/
        $(document).delegate('#forgot-pass', 'click', function () {
            $("#login-register-forms").show();
            $('#jq-login-form').hide();
            $('#jq-register-form').hide();
            $('#jq-forgot-pass-form').show();
        });

        /** Click event - Add to Wishlist **/
        $(document).delegate('.link-wishlist', 'click', function () {
            $("#login-register-forms").show();
            getUrl = $(this).data("url");
            getId = $(this).data("id");
            wishlist(getUrl, getId);
        });

        /** Click event - Login Form Close Button **/
        $(document).delegate('.login-close', 'click', function () {
            $("#login-msg").html("");
            $('#jq-login-form').hide();
            $('#login-form-modal')[0].reset();
            $(".validation-advice").hide();
            $(".input-text").removeClass("validation-failed");
            $("#login-register-forms").hide();
        });

        /** Click event - Register Form Close Button **/
        $(document).delegate('.register-close', 'click', function () {
            $("#register-msg").html("");
            $('#jq-register-form').hide();
            $('#registration-form')[0].reset();
            $(".validation-advice").hide();            
            $(".input-text").removeClass("validation-failed");
            $("#login-register-forms").hide();
        });
        
        /** Click event - Forgot Password Form Close Button **/
        $(document).delegate('.forgot-close', 'click', function () {
            $('#jq-forgot-pass-form').hide();
            $('#forgot-pass-form-modal')[0].reset();
            $(".validation-advice").hide();            
            $(".input-text").removeClass("validation-failed");
            $("#login-register-forms").hide();
        });

        /** Submit event - Login Form **/

        $(document).delegate('#login-form-modal', 'submit', function (e) {
            e.preventDefault();

            var formData = {
                'login[username]': $('input[name="login[username]"]').val(),
                'login[password]': $('input[name="login[password]"]').val()
            };
            $('.art-loader').show();
            $.ajax({
                type: "POST",
                url: loginUrl,
                data: formData,
                dataType: 'json',
                success: function (response) {
                    if ((response.status).toLowerCase() == 'error') {
                        message = response.data;
                        $("#login-msg").html(message);
                        $('.art-loader').hide();
                    } else {
                        $('#jq-login-form').hide();                        
                        sessionStorage.setItem('wishlistUrl', getUrl);
                        sessionStorage.setItem('wishlistId', getId);
                        location.reload();
                    }
                }
            });
        });

        /** Submit event - Register Form **/

        $(document).delegate('#registration-form', 'submit', function (e) {
            e.preventDefault();

            var formData = {
                'firstname': $('input[name="firstname"]').val(),
                'lastname': $('input[name="lastname"]').val(),
                'email': $('input[name="register_email"]').val(),
                'password': $('input[name="password"]').val(),
                'confirmation': $('input[name="confirmation"]').val()
            };
            $('.art-loader').show();
            $.ajax({
                type: "POST",
                url: registerUrl,
                data: formData,
                dataType: 'json',
                success: function (response) {
                    if ((response.status).toLowerCase() == 'error') {
                        message = response.data;
                        $("#register-msg").html(message);
                        $('.art-loader').hide();
                    } else {
                        $('#jq-register-form').hide();
                        sessionStorage.setItem('wishlistUrl', getUrl);
                        sessionStorage.setItem('wishlistId', getId);
                        location.reload();
                    }
                }
            });
        });

        if (sessionStorage.getItem('wishlistUrl')&&sessionStorage.getItem('wishlistId')) {
            loggedIn = 1;
            wishlist(sessionStorage.getItem('wishlistUrl'), sessionStorage.getItem('wishlistId'));
            sessionStorage.removeItem('wishlistUrl');
            sessionStorage.removeItem('wishlistId');
        }

    }); //Document ready ends here


    /** Ajax Call - Add to Wishlist **/

    var wishlist = function (url, id) {
        if (loggedIn == 1) {
            url = url.replace("wishlist/index", "ajaxwishlist/index");
            url += 'isAjax/1/';
            $('.art-loader').show();
            $.ajax({
                url: url,
                dataType: 'json',
                success: function (response) {
                    if ((response.status).toLowerCase() == 'error') {
                        wishlistMessage = response.message;
                        $(".wishlist-msg").show();
                        $(".wishlist-msg span").text(wishlistMessage);
                        $('html,body').animate({scrollTop: 0}, 800);
                        // alert(response.message);
                    } else {
                        wishlistMessage = response.message;
                        $(".wishlist-msg").show();
                        $(".wishlist-msg span").text(wishlistMessage);
                        $('html,body').animate({scrollTop: 0}, 800);
                        // alert(response.message);
                    }
                    $('.art-loader').hide();
                }
            });
        }
        else {
            $('#jq-login-form').show();
        }
    };
})(jQuery);