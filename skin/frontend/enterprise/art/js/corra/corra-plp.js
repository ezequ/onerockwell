function adjustTopButton(){
    var jqTop = jQuery('.jqTop');
    if(document.body.clientWidth >= 960 && document.body.clientWidth <= 1200) {
        jqTop.css('right','15px');
    } else {
        jqTop.css('right','');
    }
    var docViewTop = jQuery(window).scrollTop();
        if (docViewTop > 100) {
            var docViewBottom = docViewTop + jQuery(window).height();
            var elemBottom = jQuery(".col2-left-layout").offset().top + jQuery(".col2-left-layout").height();
            jqTop.fadeIn();
            if ((elemBottom < docViewBottom))
            {

                jqTop.css({'position': 'absolute', 'top': jQuery(".footer-container").offset().top - jqTop.height() - 50});
            } else {
                jqTop.css({'position': 'fixed', 'top': ''});
            }

        } else {
            jqTop.fadeOut();
        }
}

function moveToTop() {
    if(document.body.clientWidth > 997){
         jQuery('html, body').animate({
            scrollTop: 0
         }, 500);
    }
}

(function($) { //create closure so we can safely use $ as alias for jQuery
    $(document).ready(function() {
        
        $('.products-grid li.item:nth-child(4n+5)').addClass('dRow');
	//jQuery('.catblocks li:nth-child(4n+5)').addClass('tRow');
        $('.products-grid li.item:nth-child(2n+3)').addClass('mRow');
        $('.products-grid li.item:odd').addClass('even');
        $('.products-grid li.item:even').addClass('odd');
        
        
        $(document).delegate('.jqSelectedOption', 'click', function() {
            $(this).toggleClass('expanded');
            $(".jqSortByTabMob").slideToggle();
        });
        $(document).delegate('.jqLoad', 'click', loadNext);
        $(document).delegate('.jqTop', 'click', function() {
            jQuery('html, body').animate({
                    scrollTop: 0
                }, 500);
        });
        
        if($('.brands-filter').find('.active').length === 1) {
            $('.series-filter').show();
        } else {
            $('.series-filter').hide();
        }
        if(($('.pages li.current').next('li').length > 0) && (!$('.pages li.current').next('li').find('a').hasClass('next'))){
            var nextUrl = $('.pages li.current').next('li').find('a').attr('href');
            $('.jqLoad').attr('href', nextUrl);
        } else {
            $('.jqload-block').hide();
        } 
        $(".jqSelectedOptionText").text($('.jqSortByTabMob').find('a.active').text());
    });
    
    $(window).scroll(function () {
        adjustTopButton();
    });
    
    $( window ).resize(function() {
        adjustTopButton();
    });
    
    function loadNext() {
        var nextPageUrl = $(this).attr('href');
        var currentURL = document.URL;
        var isSecure = currentURL.search("https");
        if(isSecure === 0){
            nextPageUrl = nextPageUrl.replace('http','https');
        }
        $('#overlay-fixed').show();
        $.ajax({
            url: nextPageUrl,
            dataType: 'html',
            type : 'get',
            success: function(data){
                 var response = $('<div/>').html(data);
                 var productsList = response.find('.jqProductsList').html();
                 $('.jqProductsList').append(productsList);
                 if((response.find('li.current').next('li').length > 0) && (!response.find('li.current').next('li').find('a').hasClass('next'))){
                     var nextLink = response.find('li.current').next('li').find('a').attr('href');
                     $('.jqLoad').attr('href', nextLink);
                 } else {
                     $('.jqload-block').hide();
                 }
                 $('.amount').html(response.find('.amount').html());
                 //updateCopmareIcons();
                  $('.products-grid li.item:nth-child(4n+5)').addClass('dRow');
                  //jQuery('.catblocks li:nth-child(4n+5)').addClass('tRow');
                  $('.products-grid li.item:nth-child(2n+3)').addClass('mRow');
                  $('.products-grid li.item:odd').addClass('even');
                  $('.products-grid li.item:even').addClass('odd');
                 $('#overlay-fixed').hide();
                 adjustTopButton();
            }
      });
      return false;
    }
})(jQuery);