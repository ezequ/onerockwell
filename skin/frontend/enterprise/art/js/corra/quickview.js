// JavaScript Document
(function($) {
    $(document).ready(function() {

       
        /*$(".quick-view").on("click", function() {
            $(".quick-viewed").removeClass("quick-viewed");
            $(this).parents("li").addClass("quick-viewed");
        });*/
        
        $(document).delegate(".quick-view", "click", function() {
            $(".quick-viewed").removeClass("quick-viewed");
            $(this).parents("li").addClass("quick-viewed");
        });

        //Previous next functionality
        $(document).delegate("#lightwindow_next", "click", function() {
            $(".quick-viewed").nextAll('li.simple-config:visible:first').find(".quick-view").trigger("click");
        });

        $(document).delegate("#lightwindow_previous", "click", function() {
            $(".quick-viewed").prevAll('li.simple-config:visible:first').find(".quick-view").trigger("click");
        });

        //close popup
        $('#close').click(function(event) {
            $("div#wait").hide();
            $('#toPopup').css('display', 'none');
            $("#toPopup").fadeOut("normal");
            $("#backgroundPopup").fadeOut("normal");
            $(".quick-view.button").hide();
            $(".clicked").removeClass("clicked");
        });
        //background close
        $("div#backgroundPopup").click(function() {
            $("div#wait").hide();
            $('#toPopup').css('display', 'none');
            $("#toPopup").fadeOut("normal");
            $("#backgroundPopup").fadeOut("normal");
            $(".quick-view.button").hide();
            $(".clicked").removeClass("clicked");
        });
    });

    //Addtocart ajax functionality
    window.quickView = function(button, miniCartUrl) {
        var productAddToCartFormQv = new VarienForm('product_addtocart_form_qv');
        if (productAddToCartFormQv.validator.validate()) {
            var form = $(button).parents('#product_addtocart_form_qv');
            try {
                if (button && button != 'undefined') {
                    button.disabled = true;
                }
                $(button).next('#wait').show();
                var formAction = form.attr('action')
                var currentURL = document.URL;
                var isSecure = currentURL.search("https");
                if(isSecure === 0){
                    formAction = formAction.replace('http','https');
                }
                $.ajax({
                    type: 'POST',
                    url: formAction,
                    data: form.serialize(),
                    dataType: 'json',
                    success: function(response) {
                        if (response.success) {
                            $("#messages_product_view").html(response.message);
                            refreshMiniCart(miniCartUrl);
                            setTimeout(function() {
                                $('#close').trigger("click");
                            }, 2000);
                        } else {
                            $("#messages_product_view").html(response.message);
                        }
                        $(button).next('#wait').hide();
                    },
                    error: function() {
                        $("#messages_product_view").html('We apologize but there was an error with your request.<br />Please try again later.');
                        $(button).next('#wait').hide();
                    }
                });
            } catch (e) {
                throw e;
            }

            if (button && button != 'undefined') {
                button.disabled = false;
            }

            return false;
        }
    }
    
    
    


    //Refreshes the minicart drop down
    window.refreshMiniCart = function(miniCartUrl) {
        var currentURL = document.URL;
        var isSecure = currentURL.search("https");
        if(isSecure === 0){
            miniCartUrl = miniCartUrl.replace('http','https');
        }
        $.ajax({
            type: 'GET',
            url: miniCartUrl,
            dataType: 'html',
            success: function(response) {
                var $response = $(response);
                var responseTitle = $response.find('#cartHeader');
                var responseContent = $response.find('#topCartContent').html();
                $('#cartHeader').replaceWith(responseTitle);
                $('#topCartContent').html(responseContent);

                // Below can be used to show minicart after item added
                window.setTimeout(delayResponse, 250);
                /* var CurrentPageUrl = document.URL;
                if(CurrentPageUrl.indexOf("checkout/cart") >= 0){
                	window.location.reload();
                } */
                function delayResponse() {
                    Enterprise.TopCart.showCart(5)
                }

            },
            error: function() {
                displayMessage('We apologize but there was an error with your request.<br />Please try again later.');
            }
        });
    }
})(jQuery);


//Quicview ajax function
function quickviewproductid(id)
{
    loading();
    var currentURL = document.URL;
    var isSecure = currentURL.search("https");
    if(isSecure === 0){
        id = id.replace('http','https');
    }
    jQuery.ajax({
        type: "POST",
        cache: false,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        url: id,
        success: function(json)
        {
            jQuery("#lightwindow_contents").html(json.content);
            jQuery('#backgroundPopup').css('display', 'block');
            jQuery('#toPopup').css('display', 'block');
            jQuery("#toPopup").fadeIn(0500); // fadein popup div
            jQuery("#backgroundPopup").css("opacity", "0.7"); // css opacity, supports IE7, IE8
            jQuery("#backgroundPopup").fadeIn(0001);

            if (jQuery(".quick-viewed").nextAll('li.simple-config:visible:first').length <= 0) {
                jQuery("#lightwindow_next").hide();
            } else {
                jQuery("#lightwindow_next").show();
            }

            if (jQuery(".quick-viewed").prevAll('li.simple-config:visible:first').length <= 0) {
                jQuery("#lightwindow_previous").hide();
            } else {
                jQuery("#lightwindow_previous").show();
            }
            jQuery.uniform.update();
            jQuery("select").uniform();
            jQuery("select").not('.no-uniform').change(function() {
                jQuery.uniform.update();
            });

            if (typeof opConfig != "undefined") {
                opConfig.reloadPrice();
            }
            $('product_addtocart_form_qv').getElements().each(function (el) {
                if (el.type == 'select-one') {
                    if (el.options && (el.options.length > 1)) {
                        el.options[0].selected = true;
                        spConfig.reloadOptionLabels(el);
                        var cheapestPid = spConfig.getProductIdOfCheapestProductInScope("finalPrice");
                        var childProducts = spConfig.getChildProducts();
                        var price = childProducts[cheapestPid]["price"];
                        var finalPrice = childProducts[cheapestPid]["finalPrice"];
                        optionsPrice.productPrice = finalPrice;
                        optionsPrice.productOldPrice = price;
                        optionsPrice.reloadqv();
                    }
                }
            });
            
        }
    });
}


//ajax loading function
function loading() {
    jQuery("div#wait").show();
}
