CorraUI = function ($, window) {
    var currentViewport, oldViewport;
    var options = {
        desktop: 1160,
        tablet: 989,
        mobile: 767
    };
    
    isAndroid = function () {
        return navigator.userAgent.match(/Android/i) ? true : false;
    };
    isBlackBerry = function () {
        return navigator.userAgent.match(/BlackBerry/i) ? true : false;
    };
    isiOS = function () {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i) ? true : false;
    };
    isWindows = function () {
        return navigator.userAgent.match(/IEMobile/i) ? true : false;
    };
    isTouch = function () {
        return (isAndroid() || isBlackBerry() || isiOS() || isWindows());
    };
    checkMediaQuery = function (width) {
        if (navigator.appName != 'Microsoft Internet Explorer') {
            mql = window.matchMedia("screen and (max-width: " + width + "px)");
            return mql.matches;
        } else {
            return $(window).width() <= width;
        }
    };
    isTablet = function () {
        return checkMediaQuery(options.tablet);
    };
    isMobile = function () {
        return checkMediaQuery(options.mobile);
    };
    isDesktop = function () {
        return !(isTablet(options.tablet) || isMobile(options.mobile));
    };
    init = function (settings) {
        //Merge custom options
        $.extend(options, settings);
        updateViewport();
        if (isTouch()) {
            $("body").addClass("touch");
        } else {
            $("body").addClass("desktop");
        }
        $("body").addClass(currentViewport + "-width");
        $(window).resize(function () {
            oldViewport = currentViewport;
            updateViewport();
            if(oldViewport !== currentViewport){
                $("body").removeClass("mob-width tab-width desk-width").addClass(currentViewport + "-width");
            }
        });
    };
    updateViewport = function () {
        if (isMobile()) {
            currentViewport = "mob";
        } else if (isTablet()) {
            currentViewport = "tab";
        } else {
            currentViewport = "desk";
        }
        
    };
    getViewport = function(){
        return currentViewport;
    };
    viewportChanged = function () {
        return oldViewport !== currentViewport;
    };
    return this;
}(jQuery,window);