/**
 * Method to update filters
 * @param {object} filteredNavObj
 * @returns {null}
 */
var updateNav = function(filteredNavObj){
    j = jQuery;

    j( "li.jqFilterOptions" ).each(function( index ) {
        var myID = j(this).attr("id");

        if(j(filteredNavObj).find("#"+myID).length > 0){
            j(this).replaceWith(j(filteredNavObj).find("#"+myID));
            
        }else{
         var filLink = j(this).find("a");
             j(this).html(filLink.html()).addClass('disabled');
            j(this).find(".item-count").text('');
        }
    }); 
    j(".jqSelectedOptionText").text(j('.jqSortByTabMob').find('a.active').text());
    if(j(filteredNavObj).find("#selected-filters").length <= 0){
        j("#selected-filters").html("");
    }
    if(j("#selected-filters").length > 0){
        j("#selected-filters").replaceWith(j(filteredNavObj).find("#selected-filters"));
    }else{
        j(filteredNavObj).find("#selected-filters").insertAfter(".block-layered-nav");
    }

};
var Cpager=Class.create();
Cpager.prototype={
	initialize:function(){
		this.request=null;
		this.url=null;
		this.pop=false;
		document.observe("dom:loaded",this.olinks.bind(this));
                //this.riff(location.href, 'init');
                var currentUrl = document.URL;
                if(currentUrl.search('=') > 0){
                    this.riff(location.href, 'init');
                }
	},
	riff:function(u, i){
                var currentURL = document.URL;
                var isSecure = currentURL.search("https");
                if(isSecure === 0){
                    u = u.replace('http','https');
                }
                if(i == 'filter') {
                    this.showSpinner();
                }
		this.url=u;
                if(u.indexOf('ajax=true') < 0) {
            if(u.indexOf('?') > 0) {
                u = u + "&ajax=true";
            } else {
                u = u + "?ajax=true";
            }
        }
		if(this.request!==null){
			this.request.abort()
		}
		this.request=new Ajax.Request(u,{
			method:'get',
			onSuccess:this.glist.bind(this),
			onComplete:this.update.bind(this)
		})
	},
        update: function() {
        this.olinks();
        moveToTop();
        adjustTopButton();
        },
	getResult:function(event){
		 if(!checkMediaQuery(767)){
			jQuery('.filter-body').hide();
			jQuery('.filter-item.expanded').removeClass('expanded');
		}
		sEle=Event.element(event);
		url='';
		if(sEle.value){
			url=sEle.value
		}else if(sEle.href){
			url=sEle.href
		}else{
			sEle=Event.findElement(event,'a');
			url=sEle.href
		}
		this.riff(url, 'filter');
		event.stop();
	},
	olinks:function(){
		$$('.pages li a','.view-mode a','.sorter a:not(.no-ajax)').invoke('observe','click',this.getResult.bind(this));
		$$('.limiter select','.sorter select').invoke('removeAttribute','onchange');
		$$('.limiter select','.sorter select').invoke('observe','change',this.getResult.bind(this));
		$$('.block-layered-nav a:not(.no-ajax)').invoke('observe','click',this.getResult.bind(this));

	},
	glist:function(transport){
		ft=transport.responseText;
		if(typeof(history.pushState)=='function'){
			if(this.pop===false){
                            var newUrl = this.url;
                            newUrl = newUrl.replace("ajax=true", "").replace("?&", "?");
				history.pushState({url:this.url},document.title,newUrl)
			}else{
				this.pop=false
			}
		}
		var bagEle=new Element('div');
		bagEle.innerHTML = ft;
		var plist=bagEle.select('div.category-products')[0];
                var sortBy=bagEle.select('div#sort-by')[0];
                var viewMode=bagEle.select('p#view-mode')[0];
		var lnav=bagEle.select('div.block-layered-nav')[0];
                var pager=bagEle.select('div.pager')[0];
                jQuery('.category-products').html(plist.innerHTML);
                jQuery('#sort-by').html(sortBy.innerHTML);
                jQuery('#view-mode').html(viewMode.innerHTML);
                jQuery('.pager').html(pager.innerHTML);
                //jQuery('.category-products').removeClass('page-loading');
                updateNav(lnav);
                if((jQuery('.toolbar').find('li.current').next('li').length > 0) && (!jQuery('.toolbar').find('li.current').next('li').find('a').hasClass('next'))){
                     var nextLink = jQuery('.toolbar').find('li.current').next('li').find('a').attr('href');
                     jQuery('.jqLoad').attr('href', nextLink);
                } else {
                     jQuery('.jqload-block').hide();
                }
                jQuery('.block-layered-nav').removeClass('page-loading');
                
                jQuery(".filter-item").each(function(){
                    var fltrd_Text = "";
                    var comma = "";
                    jQuery(this).find('ol.layerednavigation li a.active').each(function(){
                       var c = jQuery(this);
                       c.find('span').remove();
                       if(c.find('img').length > 0){
                           fltrd_Text+= comma + jQuery.trim(c.find('img').attr('alt'));
                       }else{
                        fltrd_Text += comma + jQuery.trim(c.text());
                       }
                       comma = ", ";
                    });
                    if(fltrd_Text != ''){
                        fltrd_Text = " (" + fltrd_Text + ")";
                    }
                    jQuery(this).find(".jq-selected-filters-mob").html(fltrd_Text);
                    
                });
                jQuery('.products-grid li.item:nth-child(4n+5)').addClass('dRow');
                  //jQuery('.catblocks li:nth-child(4n+5)').addClass('tRow');
                jQuery('.products-grid li.item:nth-child(2n+3)').addClass('mRow');
                jQuery('.products-grid li.item:odd').addClass('even');
                jQuery('.products-grid li.item:even').addClass('odd');
                jQuery('#overlay-fixed').hide();
	},
	showSpinner:function(){
                jQuery('#overlay-fixed').show();
	}
};
Object.extend(Ajax);
Ajax.Request.prototype.abort=function(){
	this.transport.onreadystatechange=Prototype.emptyFunction;
	this.transport.abort();
	Ajax.activeRequestCount--
};
var ajaxPager=new Cpager();