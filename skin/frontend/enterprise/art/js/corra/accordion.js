(function ($) {

    $.fn.accordion = function (settings) {

        /* Cutomizable Options 
         ----------------------------------------------------------------------------*/
        var options = $.extend({
            "head": '.jq-accordion-head',
            "body": '.jq-accordion-body',
            "group": '.jq-accordion-group',
            "openedClass": 'open',
            "defaultOpen": false,
            "multiple": false
        }, settings);

        $(this.selector).each(function () {
            $(this).find(options.head).off('.accordion')
                    .on('click.accordion', toggleAccordion);
            $(this).find(options.body).hide();
            if(options.defaultOpen){
                var first = options.body +':first';
                $(this).find(first).fadeIn().prev(options.head).addClass(options.openedClass).siblings(options.head).removeClass(options.openedClass);
            }
        });
        this.destroy = function(){
            $(this).find(options.head).off('.accordion');
            $(this).find("."+options.openedClass).removeClass(options.openedClass);
            $(this).find(options.body).css({"display":""});
        };
        function toggleAccordion(e) {
            var _this = $(this);
            if (options.multiple === false) {
                var myParent = _this.closest(options.group);
                var myBody = _this.next(options.body);
                myParent.find(options.head).not(_this).removeClass(options.openedClass);
                myParent.find(options.body).not(myBody).slideUp();
                _this.toggleClass(options.openedClass);
                myBody.slideToggle();
            } else {
                _this.toggleClass(options.openedClass);
                _this.next(options.body).slideToggle();
            }
            return false;
        }
        return this;
    };
})(jQuery);

