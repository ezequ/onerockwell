/**
 * For swapping images based on the device
 * @param {type} $
 * @returns {undefined}
 */
//TODO: on resize add isViewPortChanged checking
//TODO: detact device type getViewPort => var viewPort = 'mob'; 
;
(function ($) {
    $.fn.corraResponsiveImage = function (options) {
        var el = $(this);
        var opts = $.extend($.fn.corraResponsiveImage.defaults, options);
        $(this).each(function () {
            var viewPort = CorraUI.getViewport();
             //console.log(opts[viewPort])
            if (typeof opts[viewPort] != 'undefined') { 
                if (typeof $(this).data(opts[viewPort]) != 'undefined') {
                  
                    $(this).attr('src', $(this).data(opts[viewPort]));
                }
            }
        });
        $(window).resize(function () {
            if (CorraUI.viewportChanged()) {
                el.corraResponsiveImage(opts);
            }
        });
        return this;
    };
    $.fn.corraResponsiveImage.defaults = {
        mob: "src-mob",
        tab: "src-tab",
        desk: "src-desk"
    };
})(jQuery);