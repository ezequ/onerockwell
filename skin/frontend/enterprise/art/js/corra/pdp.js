(function ($) {
    $(document).ready(function () {
        var breakpoints = {
            0: {
                items: 1
            },
//        360: {
//            items: 1
//        },
            767: {
                items: 3,
                autoWidth: true
            },
            1160: {
                items: 3,
                autoWidth: true
            }
        };


        var carouselInit = function () {
            if (!CorraUI.isMobile()) {
                $(".zoom").show();
                if (galleryImagesCount === 2) {
                    breakpoints[1160].items = 1;
                    breakpoints[767].items = 1;
                    breakpoints[1160].autoWidth = false;
                    breakpoints[767].autoWidth = false;
                }
            } else {
                $(".zoom").hide();
            }
        };

        var initResize = function () {
            if (CorraUI.viewportChanged()) {
                carouselInit();
            }
            
            if (CorraUI.checkMediaQuery(767)) {
                $("#jq-slider-container").insertAfter(".jq-aai-head.open");
                $(".short-description").insertAfter(".order-tab:first");
            } else {
                $(".short-description").appendTo("#jq-product-name");
                $("#jq-slider-container").insertAfter(".also-available-in .headers");
            }
        };

        var initScroll = function () {
            //stickyHeader();
        };

        var buildAAIslider = function () {
            var _this = $(this);
            if(_this.hasClass("open")){
                return false;
            }
            _this.siblings().removeClass('open');
            _this.addClass('open');
            
            $("#jq-owl-images").remove();
            //Creating new gallery elements
            var newOwlImages = $("<div>", {"id": "jq-owl-images"});
            var id = _this.data("id");


            if (typeof aaiGalleryJson[id] !== 'undefined') {
                var galleryData = aaiGalleryJson[id];
            }

            /*
             {
             "img": "http://art.loc/media/catalog/product/cache/1/thumbnail/400x/9df78eab33525d08d6e5fb8d27136e95/s/q/sq-sbcambmx12_1.jpg",
             "title": "Lorem ipsum dolor sit amet",
             "subtitle": "Sub Lorem ipsum dolor sit amet",
             "price": "$18.00 per sq.ft",
             "productId": "21",
             "productUrl": "http://art.loc/index.php/azul-macaubas-1"
             }
             */
            galleryData.each(function (value) {
                var item = $("<div>", {"class": "item"});

                var html = '';
                html += '<div class="product-image-wrapper">';
                html += '    <a href="' + value.productUrl + '" title="' + value.title + '" class="product-image"><img src="' + value.img + '" alt="' + value.title + '"></a>';
                html += '    <a href="javascript:;" data-url="' + value.productUrl + '" data-id="' + value.productId + '" class="link-wishlist">Add to Wishlist</a>';
                html += '</div>';
                html += '<div class="product-info-wrapper">';
                html += '    <h2 class="product-name"><a href="' + value.productUrl + '" title="' + value.title + '">' + value.title + '</a></h2>';
                html += '    <div class="product-info">' + value.subtitle + '</div>';
                html += '    <div class="price-box">';
                html += '       <span class="regular-price" id="product-price-' + value.productId + '">';
                html += '       <span class="price">' + value.price + '</span>';
                html += '    </div>';
                html += '</div>';



                item.append(html);
                newOwlImages.append(item);
            });
            //New gallery append to modal
            $("#jq-slider-container").append(newOwlImages);
            if(CorraUI.checkMediaQuery(767)){
                $("#jq-slider-container").insertAfter(_this);
            }else{
                $("#jq-slider-container").insertAfter(".also-available-in .headers");
            }
            
            newOwlImages.owlCarousel({"items": 4, "nav": true, "dots": false, navRewind: false, responsive:{
            0: {
                items: 2,
                "dots": true
            },
            767: {
                items: 3
            },
            1160: {
                items: 4
            }
        }});
        };
        var init = function () {
            initResize();
            carouselInit();
            $(window).resize(initResize).scroll(initScroll);

            if (galleryImagesCount >= 2) {
                $('#main-image-slider').owlCarousel({
                    dots: false,
                    lazyLoad: false,
                    nav: true,
                    autoheight: true,
                    transitionStyle: "fade",
                    responsive: breakpoints
                });
            }

            $(".order-tab").tab();

            $(".jq-share-btn").click(function () {
                $(".social-links-wrapper").toggleClass('open');
            });

            $("#jq-product-name").click(function () {
                $(".product-shop").toggleClass("show-short-description");
            });
            $(".jq-aai-head").click(buildAAIslider);
            $(".jq-aai-head:first").trigger("click");
            $(".product-view").accordion({"multiple": true});
            
            
            $("#jq-related-products").owlCarousel({"items": 4, "nav": true, "dots": false, navRewind: false, responsive:{
            0: {
                items: 2,
                "dots": true
            },
            767: {
                items: 3
            },
            1160: {
                items: 4
            }
        }});
            
        }; 
        init();
    });
})(jQuery);

        